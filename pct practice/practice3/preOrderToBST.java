package practice3;

import java.util.ArrayList;
import java.util.Scanner;

//import PrintingNames.BinarySearchTree.Node;

public class preOrderToBST {

	class Node{
		int name;
		Node leftChild;
		Node rightChild;
		Node(int name){
			this.name=name;
		}
	}
	Node root;
	static int x=0;
	
	public Node constructTree(int[] pre, int[] ino, int pstrt, int pend, int istrt, int iend) {
		if(istrt>iend)
			return null;
		Node temp=new Node(pre[x++]);
		if(istrt==iend)
			return temp;
		int index=searchIndex(ino, istrt, iend, temp.name);
		
		temp.leftChild=constructTree(pre, ino, pstrt+1, pstrt+index, istrt, index-1);
		temp.rightChild=constructTree(pre, ino, pstrt+index+1, pend, index+1, iend);
		
		return temp;
	    
		
	}
	
	public int searchIndex(int[] ino, int istrt, int iend, int value) {
		int i;
		for(i=istrt;i<=iend;i++) {
			if(ino[i]==value)
				return i;
		}
		return i; 
	}
	
	static String postOrder="";
	public void postOrder(Node root) {
		if(root!=null) {
			postOrder(root.leftChild);
			postOrder(root.rightChild);
			postOrder+=root.name+" ";
		}
	}
	
	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		String temp1= sc.nextLine();
		String[] preOrder=temp1.split("\\s+");
		int[] actualPre=new int[preOrder.length];
		ArrayList<Integer> preOrderList=new ArrayList<>();
		for(int i=0;i<preOrder.length;i++) {
			actualPre[i]=Integer.parseInt(preOrder[i]);
			preOrderList.add(actualPre[i]);		
		}
		preOrderList.sort(null);
		int[] inOrder=new int[preOrderList.size()];
		for(int i=0;i<preOrderList.size();i++)
			inOrder[i]=preOrderList.get(i);
		
		preOrderToBST ob=new preOrderToBST();
		Node root=ob.constructTree(actualPre, inOrder, 0, actualPre.length-1, 0, inOrder.length-1);
		ob.postOrder(root);	
		ob.postOrder.trim();
		System.out.println(ob.postOrder);
	}

}
