package practice3;

import java.util.Scanner;
import java.util.regex.Pattern;

import org.omg.CosNaming.IstringHelper;

public class PreInoToBinary {

	class Node{
		String name;
		Node leftChild;
		Node rightChild;
		Node parent;
		Node(String name){
			this.name=name;
		}
	}
	Node root;
	static String postOrder="";
	public Node constructTree(String[] pre, String[] ino, int pstrt, int pend, int istrt, int iend) {
		if(istrt>iend)
			return null;
		Node temp=new Node(pre[pstrt]);
		if(istrt==iend)
			return temp;
		int i= searchIndex(ino, istrt, iend, temp.name);
		
		temp.leftChild=constructTree(pre, ino, pstrt+1, pstrt+i, istrt, i-1);
		temp.rightChild=constructTree(pre, ino, pstrt+1+i , pend, i+1, iend);
		
		return temp;
		
	}
	
	public int searchIndex(String[] ino, int istrt, int iend, String value) {
		int i;
		for(i=istrt; i<=iend;i++) {
			if(ino[i].equalsIgnoreCase(value))
				return i;
		}
		return i;
	}
	
	public void printInorder(Node root) {
		if(root!=null) {
			printInorder(root.leftChild);
			printInorder(root.rightChild);
			postOrder+=root.name+" ";
		}
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String temp1=sc.nextLine();
		String[] preOrderArr=temp1.split("\\s+");
		String temp2=sc.nextLine();
		String[] inOrderArr= temp2.split("\\s+");
		
		PreInoToBinary ob=new PreInoToBinary();
		Node root=ob.constructTree(preOrderArr, inOrderArr, 0, preOrderArr.length-1, 0, inOrderArr.length-1);
		
		ob.printInorder(root);
		ob.postOrder.trim();
		System.out.println(postOrder);
		
		
	}
	
	

}
