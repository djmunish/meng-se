package practice3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//import PrintingNames.ExactSameTree.Node;

public class RootToLeafSum {

	public class Node{
		int name;
		Node parent;
		Node leftChild;
		Node rightChild;
		Node(int x){
			name=x;
		}
	}
	
	Node root;
	List<Integer> result=new ArrayList<>();
	public void addNode(int parent, int child) {		
		Node childtemp=new Node(child);
		if(root==null) {
			root=new Node(parent);
		}
		Node parentTemp=findNode(root, parent);
		if(parentTemp.leftChild==null) {
			parentTemp.leftChild=childtemp;
			childtemp.parent=parentTemp;
		}
		else {
			parentTemp.rightChild=childtemp;
			childtemp.parent=parentTemp;
		}
	}
	
	public Node findNode(Node root, int y) {
		if(root==null) {
			return null;
		} 
		if(root.name == y) {
			return root;
		}
		else {
			Node tempLeft=findNode(root.leftChild,y);
			if(tempLeft==null) {
				Node tempRight=findNode(root.rightChild, y);
				return tempRight;
			}
			return tempLeft;
		}
	}
	
	public boolean rootToLeafSum(Node root, int sum, List<Integer> result) {
		if(root==null)
			return false;
		if(root.leftChild==null && root.rightChild==null) {
			if(root.name==sum) {
				result.add(root.name);
				return true;
			}
			else
				return false;
		}
		if(rootToLeafSum(root.leftChild, sum-root.name, result)) {
			result.add(root.name);
			return true;
		}
		if(rootToLeafSum(root.rightChild, sum-root.name, result)) {
			result.add(root.name);
			return true;
		}
		return false;
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int noOfLines= Integer.parseInt(sc.nextLine());
		RootToLeafSum ob=new RootToLeafSum();
		for(int i=0;i<noOfLines;i++) {
			String temp=sc.nextLine();
			String[] tempArr=temp.split("\\s+");
			int[] actualNos=new int[tempArr.length];
			for(int j=0;j<actualNos.length;j++) 
				actualNos[j]=Integer.parseInt(tempArr[j]);
			
			ob.addNode(actualNos[0],actualNos[1]);
		}
		
		int sum= Integer.parseInt(sc.nextLine());
		
		if(ob.rootToLeafSum(ob.root, sum, ob.result)) {
			System.out.println("Yes");
			String x="";
			for(int i=ob.result.size()-1;i>=0;i--) {
			    x+=ob.result.get(i)+" ";
			}
			x.trim();
			System.out.println(x);
		}
		else
			System.out.println("No");
		
	}
}
