package practice3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;

//import PrintingNames.RootToLeafSum.Node;

public class printBFS {
	

	public class Node{
		int name;
		Node parent;
		Node leftChild;
		Node rightChild;
		Node(int x){
			name=x;
		}
	}
	
	Node root;
	List<Integer> result=new ArrayList<>();
	public void addNode(int parent, int child) {		
		Node childtemp=new Node(child);
		if(root==null) {
			root=new Node(parent);
		}
		Node parentTemp=findNode(root, parent);
		if(parentTemp.leftChild==null) {
			parentTemp.leftChild=childtemp;
			childtemp.parent=parentTemp;
		}
		else {
			parentTemp.rightChild=childtemp;
			childtemp.parent=parentTemp;
		}
	}
	
	public Node findNode(Node root, int y) {
		if(root==null) {
			return null;
		} 
		if(root.name == y) {
			return root;
		}
		else {
			Node tempLeft=findNode(root.leftChild,y);
			if(tempLeft==null) {
				Node tempRight=findNode(root.rightChild, y);
				return tempRight;
			}
			return tempLeft;
		}
	}
	
	public void BFS(Node root) {
		if(root==null)
			return;
		Queue<Node> q =new LinkedList<>();
		q.offer(root);
		q.offer(null);
		ArrayList<Integer> x= new ArrayList<>();
		while(!q.isEmpty()) {
			root=q.poll();
			if(root!=null) {
				//System.out.print(root.name+" ");
				x.add(root.name);
				if(root.leftChild!=null)
					q.offer(root.leftChild);
				if(root.rightChild!=null)
					q.offer(root.rightChild);
			}
			else {
				if(!q.isEmpty()) {
			    System.out.print(x.size());
				System.out.println();
				x.clear();
				q.offer(null);
				}
				else
					 System.out.print(x.size());
			}
				
		}	
	}

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int noOfLines= Integer.parseInt(sc.nextLine());
		printBFS ob=new printBFS();
		for(int i=0;i<noOfLines;i++) {
			String temp=sc.nextLine();
			String[] tempArr=temp.split("\\s+");
			int[] actualNos=new int[tempArr.length];
			for(int j=0;j<actualNos.length;j++) 
				actualNos[j]=Integer.parseInt(tempArr[j]);
			
			ob.addNode(actualNos[0],actualNos[1]);
		}
		
		ob.BFS(ob.root);

	}

}
