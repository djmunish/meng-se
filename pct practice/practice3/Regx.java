package practice3;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;

public class Regx {

	TreeMap<Integer, String > tm=new TreeMap<>();
	static int x=0,y=0;
	static ArrayList<String> result=new ArrayList<>();
	public void generateSubstrings(String actual) {
		for(int i=0;i<actual.length();i++)
			for(int j=i+1;j<=actual.length();j++) {
				tm.put(x,actual.substring(i, j));
				x++;
			}
	}
	
	public String findSolution(String toFind) {
		char[] find=toFind.toCharArray();
		for(int i=0;i<tm.keySet().size();i++) {
			char[] toCheck=tm.get(i).toCharArray();
			int x=0;
			for(int k=0;k<find.length;k++) {
				for(int j=0;j<toCheck.length;j++) {
					if(toCheck[j]==find[k]) {
						x++;
						continue;
					} 
				}
			}
			if(x>=toFind.length()) {
				result.add(tm.get(i));
				y++;
			}
		}
		String temp=null;
		if(result!=null) {
		temp=result.get(1);
		for(int i=0;i<result.size();i++) {
			if(temp.length()>result.get(i).length())
				temp=result.get(i);
			
		}
		}
	return temp;
	
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Regx r=new Regx();
		String str= sc.nextLine();
		String letters=sc.nextLine();
		if(str.matches("[a-zA-Z0-9,. ]+") && str.length()<=70)
			r.generateSubstrings(str);
		if(letters.length()<str.length()) {
			String result=r.findSolution(letters);
			System.out.println(result);
		}
			
	}

}
