package practice2;

import java.util.ArrayList;
import java.util.Scanner;

import pct.FamilyTree.Node;

public class LowComAncBT {

	public class Node{
		String name;
		Node parent;
		Node leftChild;
		Node rightChild;
		Node(String x){
			name=x;
		}
	}
	
	Node root;
	static String x="";
	public void addNode(String parent, String child) {
		Node childtemp=new Node(child);
		if(root==null) {
			root=new Node(parent);
		}
		Node parentTemp=findNode(root, parent);
		if(parentTemp.leftChild==null) {
			parentTemp.leftChild=childtemp;
			childtemp.parent=parentTemp;
		}
		else {

			parentTemp.rightChild=childtemp;
			childtemp.parent=parentTemp;
		}
	}
	
	public Node findNode(Node root, String y) {
		if(root==null) {
			return null;
		} 
		if(root.name.equalsIgnoreCase(y)) {
			return root;
		}
		else {
			Node tempLeft=findNode(root.leftChild,y);
			if(tempLeft==null) {
				Node tempRight=findNode(root.rightChild, y);
				return tempRight;
			}
			return tempLeft;
		}
	}
	
	public Node lowComAnc(Node root, String n1, String n2) {
		if(root==null)
			return null;
		if(root.name.equalsIgnoreCase(n1) || root.name.equalsIgnoreCase(n2))
			return root;
		Node left= lowComAnc(root.leftChild, n1, n2);
		Node right= lowComAnc(root.rightChild, n1, n2);
		if(left!=null && right!=null)
			return root;
		if(left==null && right == null)
			return null;
		return left!=null?left:right;
		
	}
	
	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		int number=Integer.parseInt(sc.nextLine());
		LowComAncBT ob =new LowComAncBT();
		for(int i=0;i<number;i++) {
			String temp=sc.nextLine();
			String[] tempArr= temp.split("\\s+");
			ob.addNode(tempArr[0], tempArr[1]);
		}
		String temp= sc.nextLine();
		String[] tempArr= temp.split("\\s+");
		Node x= ob.lowComAnc(ob.root, tempArr[0], tempArr[1]);
		if(x!=null)
		System.out.println(x.name);
	}
}