package practice2;

import java.util.Scanner;

import pct.FamilyTree.Node;

public class ExactSameTree {

	public class Node{
		int name;
		Node parent;
		Node leftChild;
		Node rightChild;
		Node(int x){
			name=x;
		}
	}
	
	Node root1, root2;
	public void addNode(int parent, int child, int x) {
		
		if(x==1) {
		Node childtemp=new Node(child);
		if(root1==null) {
			root1=new Node(parent);
		}
		Node parentTemp=findNode(root1, parent);
		if(parentTemp.leftChild==null) {
			parentTemp.leftChild=childtemp;
			childtemp.parent=parentTemp;
		}
		else {
			parentTemp.rightChild=childtemp;
			childtemp.parent=parentTemp;
		}
		}
		if(x==2) {
			Node childtemp=new Node(child);
			if(root2==null) {
				root2=new Node(parent);
			}
			Node parentTemp=findNode(root2, parent);
			if(parentTemp.leftChild==null) {
				parentTemp.leftChild=childtemp;
				childtemp.parent=parentTemp;
			}
			else {
				parentTemp.rightChild=childtemp;
				childtemp.parent=parentTemp;
			}
		}
	}
	
	public Node findNode(Node root, int y) {
		if(root==null) {
			return null;
		} 
		if(root.name == y) {
			return root;
		}
		else {
			Node tempLeft=findNode(root.leftChild,y);
			if(tempLeft==null) {
				Node tempRight=findNode(root.rightChild, y);
				return tempRight;
			}
			return tempLeft;
		}
	}
	
	public boolean sameTree(Node root1,Node root2) {
		if(root1==null && root2==null)
			return true;
		if(root1==null || root2==null)
			return false;
		return root1.name==root2.name && sameTree(root1.leftChild, root2.leftChild) && sameTree(root1.rightChild, root2.rightChild);
	}
	
	public boolean structurallySameTree(Node root1,Node root2) {
		if(root1==null && root2==null)
			return true;
		if(root1==null || root2==null)
			return false;
		return structurallySameTree(root1.leftChild, root2.leftChild) && structurallySameTree(root1.rightChild, root2.rightChild);
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int noOfLines= Integer.parseInt(sc.nextLine());
		ExactSameTree ob=new ExactSameTree();
		for(int i=0;i<noOfLines;i++) {
			String temp=sc.nextLine();
			String[] tempArr=temp.split("\\s+");
			int[] actualNos=new int[tempArr.length];
			for(int j=0;j<actualNos.length;j++) 
				actualNos[j]=Integer.parseInt(tempArr[j]);
			
			ob.addNode(actualNos[0],actualNos[1],1);
		}
		noOfLines=Integer.parseInt(sc.nextLine());
		for(int i=0;i<noOfLines;i++) {
			String temp=sc.nextLine();
			String[] tempArr=temp.split("\\s+");
			int[] actualNos=new int[tempArr.length];
			for(int j=0;j<actualNos.length;j++) 
				actualNos[j]=Integer.parseInt(tempArr[j]);
			
			ob.addNode(actualNos[0],actualNos[1],2);
		}
		if(ob.sameTree(ob.root1, ob.root2))
			System.out.println("Yes");
		else
			System.out.println("No");
		
		if(ob.structurallySameTree(ob.root1, ob.root2))
			System.out.println("Yes");
		else
			System.out.println("No");

	}

}
