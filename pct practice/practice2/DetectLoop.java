package practice2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class DetectLoop {
	HashMap<Integer, ArrayList<Integer>> hash=new HashMap<>();
	
	public void addEdge(int x, int y) {
		if(hash.containsKey(x)) {
			ArrayList<Integer> values=hash.get(x);
			if(!values.contains(y)) {
			values.add(y);
			hash.put(x, values);}
		}
		else {
			ArrayList<Integer> values=new ArrayList<>();
			values.add(y);
			hash.put(x, values);
		}
		
	}
	
	public Boolean isDeadlock() {
		ArrayList<Integer> visited=new ArrayList<>();
		for(Integer node: hash.keySet()) {
			if(isDeadlock(node, visited, -1))
				return true;
			visited.clear();
		}	
		return false;
	}
	
	public Boolean isDeadlock(Integer n, ArrayList<Integer> visited, int parent) {
		if(visited.contains(n))
			return true;
		visited.add(n);
		ArrayList<Integer> temp=hash.get(n);
		if(temp!=null) {
			for(int i=0;i<temp.size();i++) {
				if(!visited.contains(temp.get(i))) {
					if(isDeadlock(temp.get(i),visited, n))
						return true;
				}
				else if(temp.get(i)!=parent)
					return true;
			}
		}
		return false;	
	}
	
	public int isConnected() {
		ArrayList<Integer> visited= new ArrayList<>();
		for(Integer x : hash.keySet()) {
			isConnected(x,visited);
		}
		return visited.size();
	}
	public void isConnected(int n, ArrayList<Integer> visited) {
		if(!visited.contains(n))
			visited.add(n);
		ArrayList<Integer> lst= hash.get(n);
		if(lst!=null) {
		for(Integer x: lst) {
			if(!visited.contains(x))
				isConnected(x, visited);
		}
		}
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int noOfTests= Integer.parseInt(sc.nextLine());
		DetectLoop d=new DetectLoop();
		String[] results=new String[noOfTests];
		for(int i=0;i<noOfTests;i++) {
			int noOfNodes=Integer.parseInt(sc.nextLine());
			int noOfEdges=Integer.parseInt(sc.nextLine());
			for(int j=0;j<noOfEdges;j++) {
			String temp=sc.nextLine();
			String[] tempArr=temp.split("\\s+");
			d.addEdge(Integer.parseInt(tempArr[0]), Integer.parseInt(tempArr[1]));
			}
			if(d.isDeadlock() ||  d.isConnected()!=noOfNodes) 
				results[i]="Not a tree";
			if(d.isDeadlock()==false && d.isConnected()== noOfNodes)
				results[i]="Tree";
			
			d.hash.clear();
		}
		String print="";
		for(int i=0;i<results.length;i++) {
			print+=results[i]+" ";
		}
		print.trim();
		System.out.println(print+"\n");

	}

}
