package practice2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class PathCheck {
	
HashMap<Integer, ArrayList<Integer>> hash=new HashMap<>();
HashMap<Integer, ArrayList<Integer>> reverse=new HashMap<>();
	
	public void addEdge(int x, int y) {
		if(hash.containsKey(x)) {
			ArrayList<Integer> values=hash.get(x);
			if(!values.contains(y)) {
			values.add(y);
			hash.put(x, values);}
		}
		else {
			ArrayList<Integer> values=new ArrayList<>();
			values.add(y);
			hash.put(x, values);
		}	
	}
	
	public void getTranspose() {
		for(Integer x: hash.keySet()) {
			ArrayList<Integer> temp1=hash.get(x);
			if(temp1!=null) {
				for(int i=0;i<temp1.size();i++) {
					if(reverse.containsKey(temp1.get(i))) {
						ArrayList<Integer> values=reverse.get(temp1.get(i));
						if(!values.contains(x)) {
						values.add(x);
						reverse.put(temp1.get(i), values);}
					}
					else {
						ArrayList<Integer> values=new ArrayList<>();
						values.add(x);
						reverse.put(temp1.get(i), values);
					}	
				}	
		
			}
			
		}
	}
	
	ArrayList<Integer> visited= new ArrayList<>();
	Boolean temp= false;
	static String result="";
	public boolean path(int n1, int n2) {
		if(!visited.contains(n1)) {	
			visited.add(n1);
			if(n1 == n2)
			return true;
			ArrayList<Integer> lst= hash.get(n1);
			if(lst.contains(n2)) {
				result+=n1+" ";
				return true;}
			else {
				if(lst!=null) {
					for(int i=0;i<lst.size();i++) {
						if(!visited.contains(lst.get(i))) {
							result+=n1+" ";
							temp=path(lst.get(i), n2);
							result+=n2+" ";
							break;
						}
					}
				}
			}
			return temp;
		}
		return temp;
	}

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
	//	int noOfTests= Integer.parseInt(sc.nextLine());
		PathCheck ob=new PathCheck();
		//String[] results=new String[noOfTests];
		//for(int i=0;i<noOfTests;i++) {
		int noOfLines=Integer.parseInt(sc.nextLine());
			//int noOfEdges=Integer.parseInt(sc.nextLine());
			for(int j=0;j<noOfLines;j++) {
			String temp=sc.nextLine();
			String[] tempArr=temp.split("\\s+");
			ob.addEdge(Integer.parseInt(tempArr[0]), Integer.parseInt(tempArr[1]));
			}
	//}
		int n1=Integer.parseInt(sc.nextLine());
		int n2=Integer.parseInt(sc.nextLine());
			boolean result= ob.path(n1, n2);
			System.out.println(result);
			System.out.println(ob.result.trim());
			
			ob.getTranspose();
			
}
	
}