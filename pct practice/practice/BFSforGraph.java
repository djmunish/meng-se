package practice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;

public class BFSforGraph {
	
	HashMap<Integer,ArrayList<Integer>> hash=new HashMap<>();

	public void addEdge(int node1, int node2) {
		if(hash.containsKey(node1)) {
			ArrayList<Integer> values=hash.get(node1);
			if(!values.contains(node2)) {
			values.add(node2);
			hash.put(node1, values);}
		}
		else {
			ArrayList<Integer> values=new ArrayList<>();
			values.add(node2);
			hash.put(node1, values);
		}
	}
	
	public void BFS(int begin) {
		if(hash.isEmpty())
			return;
		Queue<Integer> q=new LinkedList<>();
		ArrayList<Integer> visited=new ArrayList<>();
		q.add(begin);
		visited.add(begin);
		while(!q.isEmpty()) {
			begin=q.poll();
			System.out.print(begin+" ");
			ArrayList<Integer> lst= hash.get(begin);
			if(lst!=null) {
			for(int i=0;i<lst.size();i++) {
				int temp=lst.get(i);
				if(!visited.contains(temp)) {
					visited.add(temp);
					q.add(temp);
				}
			}
		}
		}
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int noOfLines=Integer.parseInt(sc.nextLine());
		BFSforGraph ob=new BFSforGraph();
		for(int j=0;j<noOfLines;j++) {
			String temp=sc.nextLine();
			String[] tempArr=temp.split("\\s+");
			int[] actualNos=new int[tempArr.length];
			for(int i=0;i<tempArr.length;i++)
				actualNos[i]=Integer.parseInt(tempArr[i]);
			
			ob.addEdge(actualNos[0], actualNos[1]);
		}
		Map.Entry<Integer,ArrayList<Integer>> entry = ob.hash.entrySet().iterator().next();
		int x=entry.getKey();
		ob.BFS(x);
			
	}
		

}
