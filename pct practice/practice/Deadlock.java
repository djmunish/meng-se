package practice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Deadlock {

	HashMap<Integer,ArrayList<Integer>> hash=new HashMap<>();
	
	public void addEgde(int resource, int hold, int want) {
		if(hold!=-1) {
			if(hash.containsKey(hold)) {
				ArrayList<Integer> li=hash.get(hold);
				li.add(resource);
				hash.put(hold, li);
			}
			else {
				ArrayList<Integer> li=new ArrayList<>();
				li.add(resource);
				hash.put(hold, li);
			}
		}
		if(want!=-1) {
			if(hash.containsKey(resource)) {
				ArrayList<Integer> li=hash.get(resource);
				li.add(want);
				hash.put(resource, li);
			}
			else {
				ArrayList<Integer> li=new ArrayList<>();
				li.add(want);
				hash.put(resource, li);
			}
		}
	}
	
	public Boolean isDeadlock() {
		ArrayList<Integer> visited=new ArrayList<>();
		for(Integer node: hash.keySet()) {
			if(isDeadlock(node, visited))
				return true;
		}
		return false;
	}
	
	public Boolean isDeadlock(Integer n, ArrayList<Integer> visited) {
		if(visited.contains(n))
			return true;
		visited.add(n);
		ArrayList<Integer> temp=hash.get(n);
		if(temp!=null) {
			for(int i=0;i<temp.size();i++) {
				if(isDeadlock(temp.get(i),visited))
					return true;
			}
		}
		visited.remove(visited.size()-1);
		return false;
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int noOfLines=Integer.parseInt(sc.nextLine());
		int[] node=new int[3];
		Deadlock d=new Deadlock();
		for(int i=0;i<noOfLines;i++) {
			String temp=sc.nextLine();
			String[] temparr=temp.split("\\s+");
			for(int j=0;j<temparr.length;j++) {
				node[j]=Integer.parseInt(temparr[j]);
			}
			d.addEgde(node[0], node[1], node[2]);
		}
		if(d.isDeadlock())
			System.out.println("YES");
		else
			System.out.println("NO");

	}

}
