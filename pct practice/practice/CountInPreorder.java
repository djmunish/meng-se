package practice;

import java.util.Scanner;

public class CountInPreorder {
	public int count(int num){
		if(num == 0){
            return 0;
	    }
		int T[] = new int[num+1];
	    T[0] = 1;
	    T[1] = 1;
	    for(int i=2; i <= num; i++){
            int sum = 0;
            for(int k=0; k < i; k++){
	           sum += T[k]*T[i-k-1];
            }
	        T[i] = sum;
	    }
	    return T[num];
	}
	 
	public static void main(String[] args) {
		CountInPreorder ob = new CountInPreorder();
		Scanner sc= new Scanner(System.in);
		String temp=sc.nextLine();
		String[] tempArr= temp.split("\\s+");
		int[] actualNos= new int[tempArr.length];
		for(int i=0;i<actualNos.length;i++) {
			actualNos[i]=Integer.parseInt(tempArr[i]);
		}
		int result = ob.count(actualNos.length-1);
		System.out.println(result);
	}

}
