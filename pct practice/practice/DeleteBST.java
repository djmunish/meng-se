package practice;

import java.util.Scanner;

public class DeleteBST {

	class Node{
		int value;
		Node leftChild;
		Node rightChild;
		Node parent;
		Node(int value){
			this.value=value;
		}
	}
	
	Node root;
	
	public void addNode(int num) {
		root=addNode(root,num);
	}
	
	public Node addNode(Node root,int num) {
		if(root==null) {
			root=new Node(num);
			return root;
		}
		else {
			if(root.value>num) {
				root.leftChild=addNode(root.leftChild, num);
			}
			else if(root.value<num) {
				root.rightChild=addNode(root.rightChild, num);
			}
			return root;
		}
	}
	
	public void deleteNode(int num) {
		root = deleteNode(root,num);
	}
	
	public Node deleteNode(Node root, int num) {
		if(root == null)
			return root;
		else if(root.value>num) 
			root.leftChild=deleteNode(root.leftChild, num);
		else if(root.value<num)
			root.rightChild=deleteNode(root.rightChild, num);
		else {
			if(root.leftChild==null)
				return root.rightChild;
			if(root.rightChild==null)
				return root.leftChild;
			
			root.value=minValue(root.rightChild);
			
			root.rightChild=deleteNode(root.rightChild, root.value);
				
		}
		return root;
	}
	
	public int minValue(Node root) {
		int x=root.value;
		while(root.leftChild!=null) {
			x=root.leftChild.value;
			root = root.leftChild;
		}
		return x;
	}
	static String inOrder="";
	public void inOrder(Node root) {
		if(root!=null) {
		inOrder(root.leftChild);
		inOrder+=root.value;
		inOrder(root.rightChild);
		}
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String temp=sc.nextLine();
		String[] tempArr=temp.split("\\s+");
		int[] actualNos=new int[tempArr.length];
		DeleteBST ob=new DeleteBST();
		for(int i=0;i<tempArr.length;i++) {
			actualNos[i]=Integer.parseInt(tempArr[i]);
			ob.addNode(actualNos[i]);
		}
		
		ob.deleteNode(50);
		ob.inOrder(ob.root);
		ob.inOrder.trim();
		System.out.println(ob.inOrder);

	}

}
