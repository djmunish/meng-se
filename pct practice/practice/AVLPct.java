package practice;

import java.util.Scanner;

import practice.BinarySearchTree.Node;

public class AVLPct {
	public class Node{
		int name;
		Node leftChild;
		Node rightChild;
		Node parent;
		Node(int name){
			this.name=name;
		}
	}
	
	static Node root;
	static String preOrder="";
	
	public void addNode(int value) {
		root=addNode(root, value);
	}
	
	public Node addNode(Node root,int value) {
		if(root==null) {
			root=new Node(value);
			return root;
		}
		else {
			if(value<root.name) {
					root.leftChild=addNode(root.leftChild,value);
			}
			else
					root.rightChild= addNode(root.rightChild, value);
		}
		return root;
	}
	
	public boolean isBST(Node root, int min, int max) {
		if(root==null)
			return true;
		if(root.name >max || root.name< min)
			return false;
		return isBST(root.leftChild, min, root.name) && isBST(root.rightChild, root.name, max);
	}
	
	public boolean isAVL(Node root) {
		
		if(root!=null) {
		int lht=height(root.leftChild);
		int rht=height(root.rightChild);
		if(Math.max(lht, rht)- Math.min(lht, rht)>1)
			return false;
		else {
			isAVL(root.leftChild);
			isAVL(root.rightChild);
		}
		if(Math.max(lht, rht)- Math.min(lht, rht)>1)
			return false;
		else
			return true;
		}
		return true;
	}
	
	public int height(Node root) {
		if(root == null)
			return 0;
		int leftHt= height(root.leftChild);
		int rightHt= height(root.rightChild);
		return Math.max(leftHt, rightHt)+1;
	}
	
	public void preOrder(Node root) {
		if(root!=null) {
			preOrder+=root.name+" ";
			preOrder(root.leftChild);
			preOrder(root.rightChild);
		}
	}


	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		String temp=sc.nextLine();
		String[] tempArr=temp.split("\\s+");
		int[] actualNum=new int[tempArr.length];
		AVLPct ob =new AVLPct();
		for(int i=0;i<tempArr.length;i++) {
			actualNum[i]=Integer.parseInt(tempArr[i]);
			if(actualNum[i]!=-1)
				ob.addNode(actualNum[i]);
		}
		
		if(ob.isBST(ob.root, Integer.MIN_VALUE, Integer.MAX_VALUE)) {
			if(ob.isAVL(ob.root))
				System.out.println("Yes");
			else
				System.out.println("No");
		}
		else
			System.out.println("No");
		ob.preOrder(root);
		System.out.println(preOrder.trim());

	}

}
