package PrintingNames;

import java.util.Scanner;

public class BracketProb {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		//int countstartSquare=0,countendsquare=0,countstartcurly=0,countendcurly=0,countstartsimple=0,countendsimple=0;
		int N = Integer.parseInt(sc.nextLine());
		int countstartSquare[]=new int[N],countendsquare[]=new int[N],countstartcurly[]=new int[N],countendcurly[]=new int[N],countstartsimple[]=new int[N],countendsimple[]=new int[N];
		for(int i=0;i<N;i++) {
			String temp= sc.nextLine();
		//	if(temp.matches("[a-zA-Z\\[\\]\\{\\}\\(\\)]+")) {
				if(temp.matches("[a-zA-Z[]{}()]+")) {
				for(int j=0;j<temp.length();j++) {
					if(temp.charAt(j)=='[')
						countstartSquare[i]++;
					else if(temp.charAt(j)==']')
						countendsquare[i]++;
					else if(temp.charAt(j)=='{')
						countstartcurly[i]++;
					else if(temp.charAt(j)=='}')
						countendcurly[i]++;
					else if(temp.charAt(j)=='(')
						countstartsimple[i]++;
					else if(temp.charAt(j)==')')
						countendsimple[i]++;
				}
			}
		}
		for(int i=0;i<N;i++) {
			if(countstartcurly[i]==countendcurly[i] && countstartsimple[i]==countendsimple[i] && countstartSquare[i]==countendsquare[i])
				System.out.println("TRUE");
			else 
				System.out.println("FALSE");
		}

	}

}
