package PrintingNames;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import PrintingNames.CheckBST.Node;

public class CheckAVL {
	
	public class Node{
		int name;
		Node parent;
		Node leftChild;
		Node rightChild;
		Node(int x){
			name=x;
		}
	}
	
	Node root;
	List<Integer> result=new ArrayList<>();
	public void addNode(int parent, int child) {		
		Node childtemp=new Node(child);
		if(root==null) {
			root=new Node(parent);
		}
		Node parentTemp=findNode(root, parent);
		if(parentTemp.leftChild==null) {
			parentTemp.leftChild=childtemp;
			childtemp.parent=parentTemp;
		}
		else {
			parentTemp.rightChild=childtemp;
			childtemp.parent=parentTemp;
		}
	}
	
	public Node findNode(Node root, int y) {
		if(root==null) {
			return null;
		} 
		if(root.name == y) {
			return root;
		}
		else {
			Node tempLeft=findNode(root.leftChild,y);
			if(tempLeft==null) {
				Node tempRight=findNode(root.rightChild, y);
				return tempRight;
			}
			return tempLeft;
		}
	}
	
	public boolean isBST(Node root, int min, int max) {
		if(root==null)
			return true;
		if(root.name >max || root.name< min)
			return false;
		return isBST(root.leftChild, min, root.name) && isBST(root.rightChild, root.name, max);
	}
	
	public boolean isAVL(Node root) {
		//int ht=height(root);
		if(root!=null) {
		int lht=height(root.leftChild);
		int rht=height(root.rightChild);
		if(Math.max(lht, rht)- Math.min(lht, rht)>1)
			return false;
		else {
			isAVL(root.leftChild);
			isAVL(root.rightChild);
		}
		if(Math.max(lht, rht)- Math.min(lht, rht)>1)
			return false;
		else
			return true;
		}
		return true;
	}
	
	public int height(Node root) {
		if(root == null)
			return 0;
		int leftHt= height(root.leftChild);
		int rightHt= height(root.rightChild);
		return Math.max(leftHt, rightHt)+1;
	}

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int noOfLines= Integer.parseInt(sc.nextLine());
		CheckAVL ob=new CheckAVL();
		for(int i=0;i<noOfLines;i++) {
			String temp=sc.nextLine();
			String[] tempArr=temp.split("\\s+");
			int[] actualNos=new int[tempArr.length];
			for(int j=0;j<actualNos.length;j++) 
				actualNos[j]=Integer.parseInt(tempArr[j]);
			
			ob.addNode(actualNos[0],actualNos[1]);
		}
		if(ob.isBST(ob.root, Integer.MIN_VALUE, Integer.MAX_VALUE)) {
			if(ob.isAVL(ob.root))
				System.out.println("Yes");
			else
				System.out.println("No");
		}
		else
			System.out.println("No");
		
	}

}
