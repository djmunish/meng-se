package PrintingNames;

import java.util.ArrayList;
import java.util.Scanner;

public class BinarySearchTree {

	public class Node{
		int name;
		Node leftChild;
		Node righChild;
		Node parent;
		Node(int name){
			this.name=name;
		}
	}
	
	static Node root;
	static String preOrder="";
	
	public void addNode(int value) {
		root=addNode(root, value);
	}
	
	public Node addNode(Node root,int value) {
		if(root==null) {
			root=new Node(value);
			return root;
		}
		else {
			if(value<root.name) {
					root.leftChild=addNode(root.leftChild,value);
			}
			else
					root.righChild= addNode(root.righChild, value);
		}
		return root;
	}
	
	void preOrder(Node root) {
		if(root!=null) {
			preOrder+=root.name+" ";
			preOrder(root.leftChild);
			preOrder(root.righChild);
		}
	}
	
	public Node findNode(Node root,int value) {
		if(root==null || root.name==value) 
			return root;
		else {
			if(root.name>value)
				root=findNode(root.leftChild, value);
			else
				root=findNode(root.righChild, value);
		}
		return root;
	}
	static ArrayList<Integer> singleChild= new ArrayList<>();
	public void findSingleChild(Node r) {
		if(r==null)
			return;
		if((r.leftChild != null && r.righChild == null))
			singleChild.add(r.leftChild.name);
		if((r.righChild != null && r.leftChild == null))
			singleChild.add(r.righChild.name);
		
		findSingleChild(r.leftChild);	
		findSingleChild(r.righChild);
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String temp = sc.nextLine();
		String[] tempArr = temp.split("\\s+");
		int[] actualNum = new int[tempArr.length];
		BinarySearchTree b = new BinarySearchTree();
		
		for(int i=0;i<tempArr.length;i++) {
			actualNum[i]=Integer.parseInt(tempArr[i]);
			b.addNode(actualNum[i]);
		}
//		b.preOrder(b.root);
//		System.out.println(preOrder);
		b.findSingleChild(root);
		b.singleChild.sort(null);
		String result="";
		for(int i=0;i<singleChild.size();i++) {
			result+=singleChild.get(i)+" ";
		}
		result.trim();
		System.out.println(result);		
	}

}
