package PrintingNames;

import java.util.Scanner;

public class Hashing {

	static int[] tableContent;
	String print="";
	
	void compute(int number, int size) {
		int n= number%size;
		int rem=number%10;
		int copy = number;
		while(copy!=0) {
			rem=copy%10;
			copy=copy/10;
			if(rem%2!=0) {
				 n= (n+ copy%size)%size;
				 print+=n+" ";
				if(tableContent[n]==0) {
					tableContent[n]=number;
					return;
				}
			}
			else {
				n= (n+size- copy%size)%size;
				print+=n+" ";
				if(tableContent[n]==0) {
					tableContent[n]=number;
					return;
				}
			}
		}
		if(rem%2!=0) {
			while(true) {
			 n= (n+1)%size;
			 print+=n+" ";
			if(tableContent[n]==0) {
				tableContent[n]=number;
				return;
			}
		}
		}
		else {
			while(true) {
			n= (n-1)%size;
			print+=n+" ";
			if(tableContent[n]==0) {
				tableContent[n]=number;
				return;
			}
			}
		}
	}
	
	public static void main(String[] args) {
		
		Scanner sc= new Scanner(System.in);
		int n=Integer.parseInt(sc.nextLine());
		Hashing h=new Hashing();
		tableContent=new int[n];
		String temp= sc.nextLine();
		String[] temparr=temp.split("\\s+");
		int[] actualNumbers=new int[temparr.length]; 
		for(int i=0;i<temparr.length;i++) {
			actualNumbers[i]=Integer.parseInt(temparr[i]);
			int pos= actualNumbers[i]%(n);
			if(pos!=-1) {
			h.print+=pos+" ";
			if(tableContent[pos]==0) {
				tableContent[pos]=actualNumbers[i];
			}
			else
				h.compute(actualNumbers[i], n);
			System.out.println(h.print.trim());
			h.print="";
			}
		}
	}

}
