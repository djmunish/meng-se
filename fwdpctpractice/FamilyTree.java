package PrintingNames;

import java.util.ArrayList;
import java.util.Scanner;



public class FamilyTree {
	public class Node{
		String name;
		Node parent;
		Node leftChild;
		Node rightChild;
		Node(String x){
			name=x;
		}
	}
	
	Node root;
	static String x="";
	public void addNode(String parent, String child) {
		Node childtemp=new Node(child);
		if(root==null) {
			root=new Node(parent);
		}
		Node parentTemp=findNode(root, parent);
		if(parentTemp.leftChild==null) {
			parentTemp.leftChild=childtemp;
			childtemp.parent=parentTemp;
		}
		else {

			parentTemp.rightChild=childtemp;
			childtemp.parent=parentTemp;
		}
	}
	
	public Node findNode(Node root, String y) {
		if(root==null) {
			return null;
		} 
		if(root.name.equalsIgnoreCase(y)) {
			return root;
		}
		else {
			Node tempLeft=findNode(root.leftChild,y);
			if(tempLeft==null) {
				Node tempRight=findNode(root.rightChild, y);
				return tempRight;
			}
			return tempLeft;
		}
	}
	
	public Boolean checkRelation(String x, String y, String relation) {
		String relationFound=findRelation(x,y);
		String relationGiven=relation;
		if(relationGiven.equalsIgnoreCase(relationFound)) 
			return true;
		else if( relationFound== "child" || relationFound=="parent") {
				if((relationFound.equalsIgnoreCase("child") && relationGiven.equalsIgnoreCase("descendant")) 
			|| (relationFound.equalsIgnoreCase("parent") && relationGiven.equalsIgnoreCase("ancestor")))
					return true;
				return false;
		}
		else
			return false;
		
	}
	
	public String findRelation(String a, String b) {
		Node first=findNode(root, a);
		Node second=findNode(root, b);
		String relation=null;
		if(first.parent==second) {
			relation="child";
		}
		else if(second.parent==first) {
			relation="parent";
		}
		else if(findNode(first, second.name)!=null) {
			relation="ancestor";
		}
		else if(findNode(second, first.name)!=null){
			relation="descendant";
		}
		else if(first.parent==second.parent) {
			relation="sibling";
		}
		return relation;
			
	}
	
	public void preOrderTraversal(Node root) {
		 if(root==null)
			 return;
		 x+=root.name+" ";
		 preOrderTraversal(root.leftChild);
		 preOrderTraversal(root.rightChild);
	}
	
	
	public static void main(String[] args) {
		FamilyTree ft=new FamilyTree();
		Scanner sc= new Scanner(System.in);
		int number=Integer.parseInt(sc.nextLine());
		ArrayList<String> list=new ArrayList<>();
		for(int i=0;i<number;i++) {
			String temp=sc.nextLine();
			String[] tempArr= temp.split("\\s+");
			//Node n=new Node("a");
			if(ft.root!=null && ft.findNode(ft.root,tempArr[0])!=null)
				ft.addNode(tempArr[0],tempArr[1]);
			else
			{
				list.add(tempArr[0]);
			}	
		}
		
		if(!list.isEmpty()) {
		for(String x: list) {
			
		}
			
			
			
		}
		int tests=Integer.parseInt(sc.nextLine());
		for(int i=0;i<tests;i++) {
			String temp=sc.nextLine();
			String[] tempArr= temp.split("\\s+");
			if(ft.checkRelation(tempArr[0],tempArr[2], tempArr[1])) {
				System.out.println("TRUE");
			}
			else
				System.out.println("FALSE");
		}
		
		ft.preOrderTraversal(ft.root);
		System.out.println(ft.x);
		
	}

}
