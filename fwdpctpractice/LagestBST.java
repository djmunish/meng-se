package PrintingNames;

import java.util.Scanner;

import PrintingNames.LowComAncBT.Node;

public class LagestBST {
	
	public class Node{
		int name;
		Node parent;
		Node leftChild;
		Node rightChild;
		Node(int x){
			name=x;
		}
	}
	
	Node root;
	static String x="";
	public void addNode(int parent, int child) {
		Node childtemp=new Node(child);
		if(root==null) {
			root=new Node(parent);
		}
		Node parentTemp=findNode(root, parent);
		if(parentTemp.leftChild==null) {
			parentTemp.leftChild=childtemp;
			childtemp.parent=parentTemp;
		}
		else {

			parentTemp.rightChild=childtemp;
			childtemp.parent=parentTemp;
		}
	}
	
	public Node findNode(Node root, int y) {
		if(root==null) {
			return null;
		} 
		if(root.name==y) {
			return root;
		}
		else {
			Node tempLeft=findNode(root.leftChild,y);
			if(tempLeft==null) {
				Node tempRight=findNode(root.rightChild, y);
				return tempRight;
			}
			return tempLeft;
		}
	}
	
	public MinMax largest(Node root) {
		if(root == null)
			return new MinMax();
		MinMax left=largest(root.leftChild);
		MinMax right=largest(root.rightChild);
		MinMax m= new MinMax();
		if(left.isBST==false || right.isBST==false || (left.max > root.name || right.min < root.name)) {
			m.isBST=false;
			m.size=Math.max(left.size, right.size);
			return m;
		}
		m.isBST=true;
		m.size=left.size+right.size + 1;
		m.min= root.leftChild!=null ? left.min : root.name;
		m.max = root.rightChild!=null ? right.max : root.name;
		return m;
	}

	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		int number=Integer.parseInt(sc.nextLine());
		LagestBST ob =new LagestBST();
		for(int i=0;i<number;i++) {
			String temp=sc.nextLine();
			String[] tempArr= temp.split("\\s+");
			ob.addNode(Integer.parseInt(tempArr[0]),Integer.parseInt(tempArr[1]));
		}
		
		MinMax x=ob.largest(ob.root);
		System.out.println(x.size);
	}

}
 class MinMax{
	int min,max,size;
	boolean isBST;
	MinMax(){
		min=Integer.MAX_VALUE;
		max=Integer.MIN_VALUE;
		size=0;
		isBST=true;
	}
}