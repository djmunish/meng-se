package PrintingNames;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Scanner;

public class TopologicalSort {

HashMap<String, ArrayList<String>> hash=new HashMap<>();
	
	public void addEdge(String x, String y) {
		if(hash.containsKey(x)) {
			ArrayList<String> values=hash.get(x);
			if(!values.contains(y)) {
			values.add(y);
			hash.put(x, values); }
		}
		else {
			ArrayList<String> values=new ArrayList<>();
			values.add(y);
			hash.put(x, values);
		}
	}
	
	public ArrayList<Deque<String>> topologicalSort() {
		Deque<String> stack = new ArrayDeque<>();
		ArrayList<String> visited=new ArrayList<>();
		ArrayList<Deque<String>> results= new ArrayList<>();
		for(String x: hash.keySet()) {
			if(!visited.contains(x))
				topologicalSort(x, stack, visited);
		}
		results.add(stack);
		visited.clear();
		return results;
	}
	
	public void topologicalSort(String node, Deque<String> stack, ArrayList<String> visited) {
		visited.add(node);
		ArrayList<String> lst= hash.get(node);
		if(lst!=null) {
		for(String x : lst) {
			if(!visited.contains(x))
				topologicalSort(x, stack, visited);
		}
		}
		stack.offerFirst(node);
	
	}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int noOfTests= Integer.parseInt(sc.nextLine());
		TopologicalSort ob=new TopologicalSort();
		String[] results=new String[noOfTests];
		for(int i=0;i<noOfTests;i++) {
			int noOfNodes=Integer.parseInt(sc.nextLine());
			int noOfEdges=Integer.parseInt(sc.nextLine());
			for(int j=0;j<noOfEdges;j++) {
			String temp=sc.nextLine();
			String[] tempArr=temp.split("\\s+");
			ob.addEdge(tempArr[0], tempArr[1]);
			}
		}
		ArrayList<Deque<String>> x= ob.topologicalSort();
		for(int i=0;i<x.size();i++) {
		while(!x.get(i).isEmpty())
			System.out.print(x.get(i).poll()+" ");
		System.out.println();
		}
		
}
}
