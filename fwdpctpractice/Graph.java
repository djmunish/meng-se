package PrintingNames;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Set;

public class Graph {
	HashMap<Integer, ArrayList<Integer>> hash=new HashMap<>();
	public void createGraph(int resourse, int hold, int want) {
		if(hold!=-1) {
			if(hash.containsKey(hold)) {
				ArrayList<Integer> values= hash.get(hold);
				values.add(resourse);
				hash.put(hold, values);
			}
			else {
				ArrayList<Integer> values= new ArrayList<>();
				values.add(resourse);
				hash.put(hold, values);
			}
		}
		
		if(want!=-1) {
			if(hash.containsKey(resourse)) {
				ArrayList<Integer> values= hash.get(resourse);
				values.add(want);
				hash.put(resourse, values);
			}
			else {
				ArrayList<Integer> values= new ArrayList<>();
				values.add(want);
				hash.put(resourse, values);
			}
		}
	}
	
	public boolean isDeadLock(){
		 ArrayList<Integer> visited=new ArrayList<Integer>();
		 for(Integer node:hash.keySet()){
			// System.out.println("Start node is "+ node);
			 if(isDeadLock(node,visited)){
				 return true;
			 }
		 }
		 return false;
	 }
	 
	 public boolean isDeadLock(int node,ArrayList<Integer> visited){
		 if(visited.contains(node)){
			 return true;
		 }
		 visited.add(node);
		 ArrayList<Integer> lst=hash.get(node);
		 if(lst!=null){
			// System.out.println("Elements printed by list are");
		 for(int k=0;k<lst.size();k++){
			// System.out.print(k);
			 if(isDeadLock(lst.get(k),visited)){
				 return true;
			 }
		 }
	 }
		 visited.remove(visited.size()-1); //removing last value added since there is no list associated with it
		 return false; 
	 }
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		Graph g=new Graph();
		int counter=Integer.parseInt(sc.nextLine());
		int[] node= new int[3];
		for(int i=0;i<counter;i++) {
			String temp=sc.nextLine();
			String arr[]=temp.split("\\s+");
			for(int n=0;n<arr.length;n++) {
				node[n]=Integer.parseInt(arr[n]); 
			}
			g.createGraph(node[0], node[1], node[2]);
	}
		if (g.isDeadLock())
            System.out.println("Yes");
        else
            System.out.println("No");
	
}
}
