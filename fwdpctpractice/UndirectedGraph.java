package PrintingNames;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class UndirectedGraph {

	HashMap<Integer, ArrayList<Integer>> hash=new HashMap<>();
	
	public void addEdge(int node1, int node2) {
		if(hash.containsKey(node1)) {
			ArrayList<Integer> values=hash.get(node1);
			if(!values.contains(node2)) {
			values.add(node2);
			hash.put(node1, values);}
		}
		else {
			ArrayList<Integer> values=new ArrayList<>();
			values.add(node2);
			hash.put(node1, values);
		}
		if(hash.containsKey(node2)) {
			ArrayList<Integer> values=hash.get(node2);
			if(!values.contains(node1)) {
			values.add(node1);
			hash.put(node2, values);}
		}
		else {
			ArrayList<Integer> values=new ArrayList<>();
			values.add(node1);
			hash.put(node2, values);
		}
	}
	
	public Boolean isDeadlock() {
		ArrayList<Integer> visited=new ArrayList<>();
		for(Integer node: hash.keySet()) {
			if(isDeadlock(node, visited, -1))
				return true;
			visited.clear();
		}	
		return false;
	}
	
	public Boolean isDeadlock(Integer n, ArrayList<Integer> visited, int parent) {
		if(visited.contains(n))
			return true;
		visited.add(n);
		ArrayList<Integer> temp=hash.get(n);
		if(temp!=null) {
			for(int i=0;i<temp.size();i++) {
				if(!visited.contains(temp.get(i))) {
					if(isDeadlock(temp.get(i),visited, n))
						return true;
				}
				else if(temp.get(i)!=parent)
					return true;
			}
		}
		return false;
		
	}
	
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int noOfLines=Integer.parseInt(sc.nextLine());
		UndirectedGraph ob=new UndirectedGraph();
		for(int i=0;i<noOfLines;i++) {
			String temp= sc.nextLine();
			String[] tempArr=temp.split("\\s+");
			ob.addEdge(Integer.parseInt(tempArr[0]), Integer.parseInt(tempArr[1]));
		}
		if(ob.isDeadlock())
			System.out.println("Cycle detected");
		else
			System.out.println("No cycle is found");
	}
}
