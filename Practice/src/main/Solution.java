package main;

//A Java program to count all subsets with given sum.
public class Solution
{
	
	public static String parseName (String input)
	{
		String parsedName = "";
		
		String[] nameParts = input.split("\\s+");
		
		String fName = "";
		String mName = "";
		String sName = "";
		
		switch (nameParts.length) {
		case 0:
		{
			parsedName = "";
		}
			break;
		case 1:
		{
			fName = nameParts[0];
			parsedName = fName;
		}
			break;
		case 2:
		{
			fName = nameParts[0];
			if(endsWithVowel(nameParts[1]))
			{
				parsedName = nameParts[1] + " " +fName;
			}
			
			else 
			{
				parsedName = fName + " " + nameParts[1];
			}
		}
			break;
		case 3:
		{
			fName = nameParts[0];
			mName = nameParts[1];
			sName = nameParts[2];
			
			parsedName = sName + " " + fName + " " + mName;
		}
			break;
			

		default:
			break;
		}
		
		return parsedName;
	}
	
	public static boolean endsWithVowel(String input)
	{
		if("aeiouAEIOU".indexOf(input.charAt(input.length()-1)) == -1)
		return false;
		else return true;
	}
	

}
