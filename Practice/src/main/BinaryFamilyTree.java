package main;

public class BinaryFamilyTree 
{
Node root;

public void traversePreOrder(Node node) {
    if (node != null) {
        System.out.print(node.value+" ");
        traversePreOrder(node.left);
        traversePreOrder(node.right);
    }
}

public void add(String parent, String child) 
{
	root = addRecursive(root, parent, child);
}


public Node addRecursive(Node current, String parent, String child) 
{
	
	 if(current == null)
	 {
		 current = new Node(parent);
		 current.left = new Node(child);
	     return current;
	 }
	 
     // If current node is the child
	 if(current.value.equals(child))
	 {
	     return current;
	 }

	 // If current node is the parent
	 if(current.value.equals(parent))
	 {
	    if(current.left == null)
	    {
	    	current.left = new Node(child);
	    }
	    
	    else if(current.right == null)
	    {
	    	current.right = new Node(child);
	    }
	    
	    return current;
	 }
	 
	 // If current node is neither parent nor child, look for parent and update it
	 else 
	 {
		 findAndUpdateNode(parent, current, child);
		 return current;
	 }
	 
}

public void findAndUpdateNode(String name, Node localTree, String child)
{
    Node current = localTree;

    // Visit and update the node
    if(current.value.equals(name))
    {
    	 if(current.left == null)
 	    {
 	    	current.left = new Node(child);
 	    }
 	    
 	    else if(current.right == null)
 	    {
 	    	current.right = new Node(child);
 	    }	
    }
    
    // Pre-order - go left
    if(current.left != null)
    {
       findAndUpdateNode(name, current.left, child);
    }

    // Pre-order - go right
    if(current.right != null)
    {
      findAndUpdateNode(name, current.right, child);
    }


}

public Node FindParent(Node root, Node node)
{
    if (root == null || node == null)
    {
        return null;
    }
    else if ( (root.right != null && root.right.value.equals(node.value)) || (root.left != null && root.left.value.equals(node.value)))
    {
        return root;
    }
    else
    {
        Node found = FindParent(root.right, node);
        if (found == null)
        {
            found = FindParent(root.left, node);
        }
        return found;
    }
}

public Node findNode(String name, Node localTree)
{
    Node current = localTree;

    // Visit the node
    if(current.value.equals(name))
        return current;

    // Pre-order - go left
    if(current.left != null)
    {
        return findNode(name, current.left);
    }

    // Pre-order - go right
    if(current.right != null)
    {
        return findNode(name, current.right);
    }


    return null;
}

//Returns true if a is a child of b 
boolean childOf(String a,String b)
{
Node bNode = findNode(b, root);

if(bNode!=null)
{
	if((bNode.left!=null)&&(bNode.right!=null))
	{
return (bNode.left.value.equals(a)||bNode.right.value.equals(a));	
	}
}

return false;

}



//Returns true if a is a descendant of b 
boolean descendantOf(String a,String b)
{
Node bNode = findNode(b, root);

if(bNode!=null)
{
Node aNode = findNode(a, bNode);
return aNode!=null;	
}

return false;

}

//Returns true if a is a ancestor of b 
boolean ancestorOf(String a,String b)
{
Node aNode = findNode(a, root);

if(aNode!=null)
{
Node bNode = findNode(b, aNode);
return bNode!=null;	
}

return false;

}

//Returns true if a is a sibling of b 
boolean siblingOf(String a,String b)
{
Node aNode = findNode(a, root);
Node bNode = findNode(b, root);

Node aParent = FindParent(root, aNode);
Node bParent = FindParent(root, bNode);

if((aParent!=null)&&(bParent!=null))
return aParent.equals(bParent);

else return false;
}




}



class Node
{
	String value;
	Node left;
	Node right;
	
	Node(String value) 
	{
		this.value = value;
		left = null;
		right = null;
	}

}


