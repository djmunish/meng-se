package main;

public class FamilyTreeAnalyser 
{

	public static void main(String[] args) 
	{
		BinaryFamilyTree bt = new BinaryFamilyTree();
		 
	    bt.add("Motilal","Jawahar");
	    bt.add("Jawahar","Indira");
	    bt.add("Motilal","Kamala");
	    bt.add("Indira","Sanjay");
	    bt.add("Sanjay","Varun");
	    bt.add("Indira","Rajiv");
	    bt.add("Rajiv","Priyanka");
	    bt.add("Rajiv","Rahul");
 	
	    
//	    Motilal child Jawahar 
//	    Varun descendant Indira 
//	    Priyanka sibling Varun 
//	    Sanjay child Indira 
//	    Sanjay ancestor Varun 
//	    Kamala ancestor Rahul
	    
	    
	    System.out.print(bt.childOf("Motilal", "Jawahar") + " ");
	    System.out.print(bt.descendantOf("Varun", "Indira") + " ");
	    System.out.print(bt.siblingOf("Priyanka", "Varun") + " ");
	    System.out.print(bt.childOf("Sanjay", "Indira") + " ");
	    System.out.print(bt.ancestorOf("Sanjay", "Varun") + " ");
	    System.out.println(bt.ancestorOf("Kamala", "Rahul") + " ");

	    bt.traversePreOrder(bt.root);

	}
	
}
