package main;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class Hashing 
{

	
public static void main(String[] args) 
{
	
	
	Scanner in = new Scanner(System.in);
	
	int num = in.nextInt();
	
	HashTable table = new HashTable(num);
	
	int n = in.nextInt();
	
	while(n!=-1)
	{
		table.add(n);
		n = in.nextInt();
	}
	
	table.visitedIndexes.values().forEach((ArrayList<Integer> a)->{
		String output = "";
		for (Integer i : a) 
	{
		output = output + i + " ";	
	}
		output.trim();
		System.out.println(output);
	});
	
	in.close();
}
	
}

class HashTable
{
	
int table[];
int tableSize;
public LinkedHashMap<Integer, ArrayList<Integer>> visitedIndexes = new LinkedHashMap<>();

public HashTable(int n) 
{
	tableSize = n;
	table = new int[tableSize];
}

public void add(int n)
{
	int index = n%tableSize;
    addToVisitedIndexList(n, index);		

	//If value at index is empty
	if(table[index] == 0)
	{
		table[index] = n;
		return;
	}
	
	else 
	{
		
	int operateUpon = n;
	int newIndex = index;
	int rightMostDigit = operateUpon%10;
	
	
	while(operateUpon!=0) 
	{
		rightMostDigit = operateUpon%10;
		operateUpon = operateUpon/10;
		
		newIndex = operateUpon%tableSize;

		
		
		//If index is odd move newIndex indices forward (also wrap around edges)
		if(rightMostDigit%2!=0)
		{
			newIndex = index + newIndex;
			
			//Wrap around if needed
			if(newIndex>tableSize - 1)
			{
				newIndex = newIndex - tableSize;
			}
			
		}
		//If index is even move newIndex indices backward (also wrap around edges)
		else 
		{
			newIndex = index - newIndex;
			
			//Wrap around if needed
			if(newIndex<0)
			{
				newIndex = tableSize + newIndex;
			}
			
		}
		
		
		
		
	index = newIndex;	
    addToVisitedIndexList(n, index);	
    
  //If value at index is empty
  		if(table[newIndex] == 0)
  		{
  			table[newIndex] = n;
  			return;
  		}

	}
	
	//Check if free location found, else start to probe for location in direction corresponding to the last digit removed
	if(table[index] == 0)
	{
		table[index] = n;
		
	}
	else 
	{
		
	//Determine direction corresponding to rightmost digit
	

	int counter = tableSize;
	
	while(counter>0)
	{
		counter = counter - 1;
		
		//Move right
		if(rightMostDigit%2!=0)
		{
		index = index + 1;
		}
		//Move left
		else 
		{
			index = index - 1;
		}
		
	    addToVisitedIndexList(n, index);		
		if(table[index] == 0)
		{
			table[index] = n;
			counter = 0;
		}
	}
		
	
	

		
	}
	
	}
}

void addToVisitedIndexList(int key, int index) 
{
	ArrayList<Integer> visitedForThis = visitedIndexes.get(Integer.valueOf(key));
	if(visitedForThis == null)
	{
		visitedForThis = new ArrayList<>();
	}
	visitedForThis.add(Integer.valueOf(index));
	visitedIndexes.put(Integer.valueOf(key), visitedForThis);
}

void printVisitedIndexes() 
{
	
}

}

