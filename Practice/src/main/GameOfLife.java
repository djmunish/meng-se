package main;

import java.util.Arrays;

//# denotes blank cell
//@ denotes live cell

public class GameOfLife 
{

	
public static void main(String[] args) 
{
	
String input = "3 3\n#@#\n#@@\n@#@\n4";


System.out.println(simulateLife(input));

}	
	
public static int simulateLife (String input)
{
	String[] inputArray = input.split("\\n");
	
	int r = Integer.parseInt(inputArray[0].split("\\s")[0]);
	int c = Integer.parseInt(inputArray[0].split("\\s")[1]);;
	int n = Integer.parseInt(inputArray[inputArray.length - 1]);
	String matrix = String.join("\n", Arrays.copyOfRange(inputArray, 1, inputArray.length - 1));
	
	int grid[][] = getGrid(matrix, r, c);
	int lastGeneration[][] = nthGeneration(grid, r, c, n);
	
return getAliveCellCount(lastGeneration, r, c);	
}

// Function to convert input into grid
public static int[][] getGrid (String input , int r, int c)
{
int[][] grid = new int[r][c];

String[] rows = input.split("\\n");

for(int i=0; i<rows.length; i++)
{
	for(int j=0; j<rows[i].length(); j++)
	{
		int value = rows[i].charAt(j)=='@'? 1: 0;
		grid[i][j] = value;
	}
}

return grid;
}

public static int[][] nthGeneration(int grid[][], int M, int N, int n)
{
	int finalGen[][] = grid;
	
	for (int i = 0; i < n; i++) 
	{
		finalGen = nextGeneration(finalGen, M, N);
	}
	
return finalGen;	
}

// Function to return next generation 
public static int[][] nextGeneration(int grid[][], int M, int N) 
{ 
    int[][] future = new int[M][N]; 

    // Loop through every cell 
    for (int l = 0; l < M; l++) 
    { 
        for (int m = 0; m < N; m++) 
        { 
            // finding no Of Neighbours that are alive 
            int aliveNeighbours = 0; 
            for (int i = -1; i <= 1; i++) 
                for (int j = -1; j <= 1; j++)
                {
                	if(isInBounds(l + i, m + j, M, N))
                    aliveNeighbours += grid[l + i][m + j]; 
                }

            // The cell needs to be subtracted from 
            // its neighbours as it was counted before 
            aliveNeighbours -= grid[l][m]; 

            // Implementing the Rules of Life 

            // Cell is lonely and dies 
            if ((grid[l][m] == 1) && (aliveNeighbours < 2)) 
                future[l][m] = 0; 

            // Cell dies due to over population 
            else if ((grid[l][m] == 1) && (aliveNeighbours > 3)) 
                future[l][m] = 0; 

            // A new cell is born 
            else if ((grid[l][m] == 0) && (aliveNeighbours == 3)) 
                future[l][m] = 1; 

            // Remains the same 
            else
                future[l][m] = grid[l][m]; 
        } 
    } 
    
    return future;
    
} 

public static int getAliveCellCount(int grid[][], int r, int c)
{
	int count = 0;
	
	for(int i=0;i<r;i++)
	{
		for(int j=0;j<c;j++)
		{
			count = count + grid[i][j];
		}
	}
	
	return count;
}

static boolean isInBounds (int i, int j, int M, int N)
{
return (i<M)&&(i>=0)&&(j<N)&&(j>=0);	
}

}
