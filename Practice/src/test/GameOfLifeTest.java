package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import main.GameOfLife;

public class GameOfLifeTest 
{

	@Test
	public void testInputA()
	{
		String input = "3 3\n#@#\n#@@\n@#@\n3";
		int expectedOutput = 3;
		int realOutput = GameOfLife.simulateLife(input);
		assertEquals(expectedOutput, realOutput);
	}
	
	@Test
	public void testInputB()
	{
		String input = "3 3\n#@#\n#@@\n@#@\n4";
		int expectedOutput = 4;
		int realOutput = GameOfLife.simulateLife(input);
		assertEquals(expectedOutput, realOutput);
	}
	
}
