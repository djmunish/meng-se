package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import main.Solution;

public class SolutionTest 
{

@Test
public void testInputA()
{
	String inputA = "Prithviraj Dajisaheb Chavan";
	String expectedOutputA = "Chavan Prithviraj Dajisaheb";
	String realOutputA = Solution.parseName(inputA);
	assertEquals(expectedOutputA, realOutputA);
}

@Test
public void testInputB()
{
	String inputB = "Prithviraj D. Chavan";
	String expectedOutputB = "Chavan Prithviraj D.";
	String realOutputB = Solution.parseName(inputB);
	assertEquals(expectedOutputB, realOutputB);
}

@Test
public void testInputC()
{
	String inputC = "Barack Obama";
	String expectedOutputC = "Obama Barack";
	String realOutputC = Solution.parseName(inputC);
	assertEquals(expectedOutputC, realOutputC);
}

@Test
public void testInputD()
{
	String inputD = "Michael Jackson";
	String expectedOutputD = "Michael Jackson";
	String realOutputD = Solution.parseName(inputD);
	assertEquals(expectedOutputD, realOutputD);
}

}

