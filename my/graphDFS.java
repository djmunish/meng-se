package testJava;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import java.util.Map;

public class graphDFS {

	HashMap<Integer,ArrayList<Integer>> hash = new HashMap<>();
	
	public void addEdge(int val1, int val2) {
		if (hash.containsKey(val1)) {
			ArrayList<Integer> temp = hash.get(val1);
			temp.add(val2);
			hash.put(val1, temp);
			}
		else {
			ArrayList<Integer> temp = new ArrayList<Integer>();
			temp.add(val2);
			hash.put(val1, temp);
		}
		
	}
	public void DFS(int b) {
		if (hash==null){
			return;
		}
		ArrayList<Integer> v = new ArrayList<Integer>();
		searchDFS(v,b);
	}
	 
	public void searchDFS(ArrayList<Integer>v,int b) {
		v.add(b);
		System.out.print(b + " ");
		
		ArrayList<Integer> linkList = hash.get(b);
		
		if (linkList!=null) {
			for(int x: linkList) {
				if(!v.contains(x)) {
					searchDFS(v,x);
				}
			}
		}

	}
	
	public static void main(String[] a) {
		
		Scanner s = new Scanner(System.in);
		graphDFS obj = new graphDFS();
		
		String inpLine = s.nextLine();
		int n = Integer.parseInt(inpLine);
		
		for(int i =0; i<n ; i++) {
			String inpEdge = s.nextLine();
			String ent[] = inpEdge.split("\\s+");
			obj.addEdge(Integer.parseInt(ent[0]), Integer.parseInt(ent[1]));
		}
		
		
	
		obj.DFS((int)obj.hash.keySet().toArray()[0]);
		
		s.close();
		
	}
}

