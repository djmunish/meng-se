package testJava;

import java.util.ArrayList;
import java.util.Scanner;

public class newTest {

	public class Node{
		Node l,r,p;
		String name;
		Node(String x){
			name = x;
		}		
	}
	
	Node root;
	String outx = "";
	String boolx = "";
	
	public void addNode(String p, String c) {
		Node child = new Node(c);
		if (root == null) {
			root = new Node(p);
			root.l = child;
		}
		Node temp = findNode(root,p);

	
		if (temp.l==null) {
			temp.l = child;
			child.p = temp;
		}			
		else {
			temp.r=child;
			child.p = temp;
		}
		
	}
	
	public Node findNode(Node n, String inp) {
		if(n == null) {
			return null;
		}
		if (n.name.equalsIgnoreCase(inp)) {
			return n;
		}
		else {
			Node resl = findNode(n.l,inp);
			if (resl!=null) {
				return resl;
			}
			else {
				return findNode(n.r,inp);
			}
		}
	}
	
	public void preOrder(Node b) {
		
		if (b == null){return;}
		
		outx += b.name + " ";

		preOrder(b.l);
		preOrder(b.r);
	}

	public void postOrder(Node b) {
		if (b == null){return;}
		outx+=b.name+ " ";
		postOrder(b.r);
		postOrder(b.l);
	} 
	
	
	
	boolean childOf(String a,String b)
	{
	Node bNode = findNode(root, b);

	if(bNode!=null)
	{
		if((bNode.l!=null)&&(bNode.r!=null))
		{
			if (bNode.l.name.equals(a)||bNode.r.name.equals(a)){
				boolx += "T" + " ";
				return (bNode.l.name.equals(a)||bNode.r.name.equals(a));	
			}

		}
	}
	boolx += "F" + " ";

	return false;

	}



	//Returns true if a is a descendant of b 
	boolean descendantOf(String a,String b)
	{
	Node bNode = findNode(root, b);

	if(bNode!=null)
	{
	Node aNode = findNode(bNode, a);
	if (aNode!=null) {
		boolx += "T" + " ";
		return aNode!=null;

	}
	}
	boolx += "F" + " ";

	return false;

	}

	//Returns true if a is a ancestor of b 
	boolean ancestorOf(String a,String b)
	{
	Node aNode = findNode(root, a);

	if(aNode!=null)
	{
	Node bNode = findNode(aNode, b);
	if (bNode!=null) {
		boolx += "T" + " ";
		return bNode!=null;

	}
	}
	boolx += "F" + " ";

	return false;

	}

	//Returns true if a is a sibling of b 
	boolean siblingOf(String a,String b)
	{
	Node aNode = findNode(root, a);
	Node bNode = findNode(root, b);

//	Node aParent = FindParent(root, aNode);
//	Node bParent = FindParent(root, bNode);
	if (aNode.p.equals(bNode.p)) {
		boolx += "T" + " ";

		return aNode.p.equals(bNode.p);
	}
	boolx += "F" + " ";

	return false;

	}
	
	
	
	boolean parentOf(String a,String b)
	{
	Node aNode = findNode(root, a);
	Node bNode = findNode(root, b);

//	Node aParent = FindParent(root, aNode);
//	Node bParent = FindParent(root, bNode);
	if (bNode.p.equals(aNode)) {
		boolx += "T" + " ";

		return aNode.p.equals(bNode.p);
	}
	boolx += "F" + " ";

	return false;

	}
	
	
	
	public static void main(String[] a) {
		Scanner s = new Scanner(System.in);
		ArrayList<String> list=new ArrayList<String>();

		newTest obj = new newTest();		
		int nn = Integer.parseInt(s.nextLine());
		
		for(int i =0; i<nn ; i++) {
			String inpp = s.nextLine();
			String[] arr = inpp.split("\\s+");
			if(obj.root == null) {
				obj.addNode(arr[0], arr[1]);
			}
			else if(obj.findNode(obj.root, arr[0]) != null) {
				obj.addNode(arr[0], arr[1]);
			}
			else 
			{
				list.add(inpp);
			}
		}

		if (!list.isEmpty()) {
			System.out.println(list);
//			for (String str: list) {
//				String[] pc = str.split("\\s+");
//				obj.addNode(pc[0], pc[1]);
//			}
		}
		
		obj.preOrder(obj.root);
		System.out.println(obj.outx);
//		obj.outx = "";
//		obj.postOrder(obj.root);
//		System.out.println(obj.outx);

		
		int test = Integer.parseInt(s.nextLine());
		for(int i = 0; i<test; i++) 
		{
			String inpp = s.nextLine();
			String[] arr = inpp.split("\\s+");
			
			if (arr[1].equals("child")) {
				obj.childOf(arr[0], arr[2]);
			}
			if (arr[1].equals("descendant")) {
				obj.descendantOf(arr[0], arr[2]);
			}
			if (arr[1].equals("sibling")) {
				obj.siblingOf(arr[0], arr[2]);
			}
			if (arr[1].equals("ancestor")) {
				obj.ancestorOf(arr[0],arr[2]);
			}
			if (arr[1].equals("parent")) {
				obj.parentOf(arr[0], arr[2]);
			}
		}
		
		
		
		System.out.println(obj.boolx);
		s.close();
	}
	
}
