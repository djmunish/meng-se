package testJava;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class graph {
	
	public static HashMap<Integer,ArrayList<Integer>> holds = new HashMap<>();
	public static HashMap<Integer,ArrayList<Integer>> wants = new HashMap<>();
	public static void main(String[] a) {
		
		Scanner s = new Scanner(System.in);
		int noOfLines=Integer.parseInt(s.nextLine());		
		
		for(int i=0;i<noOfLines;i++) {
			String temp=s.nextLine();
			String[] temparr=temp.split("\\s+");


			int key = Integer.parseInt(temparr[0]);
			int holdVal = Integer.parseInt(temparr[1]);
			int wantVal =Integer.parseInt(temparr[2]);

			ArrayList<Integer> hArr = new ArrayList<Integer>();
			hArr.add(holdVal);

			ArrayList<Integer> wArr = new ArrayList<Integer>();
			wArr.add(wantVal);

			if (holdVal!=-1) {
				if (!holds.containsKey(key)) {
					holds.put(key, hArr);
				}
				else {
					ArrayList<Integer> tempH = holds.get(key);
					tempH.add(holdVal);
					holds.put(key, tempH);
				}
			}

			if (wantVal!=-1) {

				if (!wants.containsKey(key)) {
					wants.put(key, wArr);
				}
				else {
					ArrayList<Integer> tempH=wants.get(key);
					tempH.add(wantVal);
					wants.put(key, tempH);

				}
			}





		}
		System.out.println("holds");

		for (Integer k : holds.keySet()) {
			System.out.println(k + " : "+ holds.get(k));
		}
		System.out.println("wants");

		for (Integer k : wants.keySet()) {
			System.out.println(k + " : "+ wants.get(k));
		}
		
		if(checkDeadlock(holds,wants))
			System.out.println("YES");
		else
			System.out.println("NO");
		
		
		
		s.close();
	}
	
	
	public static boolean checkDeadlock(HashMap<Integer,ArrayList<Integer>> hold, HashMap<Integer,ArrayList<Integer>> want) {
		boolean lock = false;
		
		for (Integer keyH : hold.keySet()) {
			for (Integer keyW : want.keySet()) {

				ArrayList<Integer> tempH=hold.get(keyH);
				ArrayList<Integer> tempW=want.get(keyW);
				for (Integer x : tempH) {
					for (Integer y : tempW) {
						if (x == y)
						{
							lock = true;
							break;
						}
					}
				}
			}
			
		}
		
		
		
		return lock;
		
	}
	
}