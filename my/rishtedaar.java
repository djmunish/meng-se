package testJava;

import java.util.ArrayList;
import java.util.Scanner;

public class rishtedaar {
public class Node{
	String value;
	Node parent, left, right;
	Node(String name){
		value = name;
	}
}
Node root;
String x="";

public void addNode(String p, String c) {
	Node child = new Node(c);
	if (root == null)
	{
		root = new Node(p);
	}
	
	Node searchd = findParent(root,p);
	
	if (searchd.left == null) {
		searchd.left = child;
		child.parent = searchd;
	}
	else {
		searchd.right = child;
		child.parent = searchd;
	} 
}

public static Node findParent(Node searchnode, String p) {
	if (searchnode==null) {
		return null;
	}
	if (searchnode.value.equals(p)) {
		return searchnode;
	}
	else {
		Node tempL = findParent(searchnode.left,p);
		if (tempL==null) {
			Node tempR = findParent(searchnode.right,p);
			return tempR;
		}
		return tempL;
	}	
}

public void preOrderTraversal(Node root) {
	 if(root==null)
		 return;
	 x+=root.value+" ";
	 preOrderTraversal(root.left);
	 preOrderTraversal(root.right);
}

public void postOrder(Node root) {
	if (root == null) {
		return;
	}
	x+=root.value + " ";
	postOrder(root.right);
	postOrder(root.left);
}


public void checkRelation(String p, String c, String rel) {
	String boolOut = "";
	
	String aRel = findRelation(p,c);
	System.out.println(aRel);
	
	if (rel.equals(aRel)) {
		boolOut += "T" + " ";
	}

	else {
		boolOut += "F" + " ";
	}
	System.out.print(boolOut);
}

public String findRelation(String p, String c) {
	Node first = findParent(root,p);
	Node sec = findParent(root,c);
	String relCheck = "";
	
	
	if (sec.parent == first) {
		relCheck = "parent";
	}
	else if (first.parent == sec.parent) {
		relCheck = "sibling";
	}
	else if(first.parent == sec) {
		relCheck = "child";
	}
	else if(findParent(first, sec.value)!=null) {
		relCheck="ancestor";
	}
	else if(findParent(sec, first.value)!=null){
		relCheck="descendant";
	}
	return relCheck;
}


public static void main(String[] a) {
	Scanner s = new Scanner(System.in);
	int relations = Integer.parseInt(s.nextLine());
	rishtedaar r = new rishtedaar();
	ArrayList<String> list=new ArrayList<String>();

	for(int i = 0;i <relations; i++)
	{
		String inp = s.nextLine();
		String[] pc = inp.split("\\s+");
		if(r.root == null) {
			r.addNode(pc[0], pc[1]);
		}
		else if(findParent(r.root, pc[0]) != null) {
			r.addNode(pc[0], pc[1]);
		}
		else 
		{
			list.add(inp);
		}
	}

	if (!list.isEmpty()) {
		for (String str: list) {
			String[] pc = str.split("\\s+");
			r.addNode(pc[0], pc[1]);
		}
	}
	r.preOrderTraversal(r.root);
	System.out.println(r.x);
	
	
	int testRel = Integer.parseInt(s.nextLine());
	for(int i = 0;i<testRel;i++) {
		String test = s.nextLine();
		String[] relsArr = test.split("\\s+");
		r.checkRelation(relsArr[0], relsArr[2], relsArr[1]);
	}
	
	s.close();
}

}
