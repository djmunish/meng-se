package testJava;

import java.util.Scanner;

public class twoDArr {

	public static void main(String[] a) {
		Scanner s = new Scanner(System.in);
		
		String rcent = s.nextLine();

		
		int r = Integer.parseInt(rcent.split("\\s+")[0]);
		int c = Integer.parseInt(rcent.split("\\s+")[1]);
		
		int[][] arrInp = new int[r][c];
		
		
		for(int i = 0; i<r; i ++) {
			String ent = s.nextLine();
			String[] arr = ent.split("\\s+");

			for(int j = 0; j<c;  j++) {
					arrInp[i][j] = Integer.parseInt(arr[j]);

			}
		}
		for(int k = 0; k<r; k ++) {
			for(int l = 0; l<c;  l++) {
				System.out.print(arrInp[k][l] + " ");
			}
			System.out.println("");
		}
		for(int k = 0; k<r; k ++) {
			sumR(arrInp, k,c);
		}	
		for(int k = 0; k<c; k ++) {
			sumC(arrInp, k,r);
		}	
		s.close();
	}
	
	public static void sumR(int[][] g, int r, int C) {
		int sum = 0;
		for (int x = 0; x<C ; x ++) {
			sum += g[r][x];
		}
		System.out.println("sum=="+sum);
	}
	
	public static void sumC(int[][] g, int c, int R) {
		int sum = 0;
		for(int y =0 ; y<R;y++) {
			sum += g[y][c];
		}
		System.out.println("sum=" + sum);
	}
	
	
}
