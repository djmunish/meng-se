package testJava;

import java.util.ArrayList;
import java.util.Scanner;

public class hashing {

	
	public static ArrayList<Integer> posArr = new ArrayList<Integer>();
    public static String printPos = "";

	    public static void main(String[] args) {
	        // write your code here

	        Scanner s = new Scanner(System.in);

	        int n = s.nextInt();

	        int[] numbers = new int[n];

	        for(int i = 0; i<n;i++){
	            int numb = s.nextInt();
	            if (numb!=-1) {
	                numbers[i] = numb;
	            }
	        }


	        
	        for(int i = 0; i < numbers.length; i++){
	            int currNum = numbers[i];
	            int pos = currNum%n;
	            
	            printPos = "";
	            
	            if (posArr.contains(pos)){
	                compute(currNum,n,pos);
	            }
	            else {
	                posArr.add(pos);
		            printPos += pos + " ";
	            }
	            	            
	            System.out.println(printPos);
	        }
	        
	        s.close();
	    }


	    public static void compute(int i, int size, int oldPos){
	        int checkEO = i;
	        int newI = i;
	        int copyI = i;
	        int newPos = oldPos;
            printPos += oldPos + " ";

	        while(copyI != 0){
	        	checkEO = checkEO%10;
	        	newI = newI/10;
		        newPos = newI%size;
	        	if (checkEO % 2 == 0) {//even
                    newPos = oldPos - newPos;

                    if (newPos < 0) {
                        newPos = size + newPos;
                    }
                } else {
                    //odd

                    newPos = oldPos + newPos;

                    if (newPos >= size) {
                        newPos = newPos - size;
                    }
    		        oldPos =  newPos;
                }
	            printPos += newPos + " ";

	            if (!posArr.contains(newPos)){
                    posArr.add(newPos);
                    return;
	            }
	           
	   
                checkEO = newI;
	            copyI = copyI/10;
	        }
	    }

}
