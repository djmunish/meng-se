package testJava;

import java.util.Scanner;

public class testArr {

	public static void main(String[] a) {

		Scanner s = new Scanner(System.in);

		int[][] arr = new int[6][6];

		for(int i = 0; i<6;i++) {
			String[] inpArr =  s.nextLine().split(" ");
			for(int j = 0; j<6;j++) {
				arr[i][j] = Integer.parseInt(inpArr[j]);
			}
		}
		
		int max = rowSum(arr,0);

		int r = 0;
		int c = 0;
		
		
		for(int i = 0; i<6;i++) {
			for(int j = 0; j<6;j++) {
				
				if (rowSum(arr,i)>max){
					max = rowSum(arr,i);
					r = i+1;
					c=0;
				}
				if (colSum(arr,j)>max){
					max = colSum(arr,j);
					c = j+1;
					r=0;
				}
			}
		}
		System.out.println("sum max==="+max+"row===="+r+"column==="+c);

		s.close();
	}
	
	public static int rowSum(int[][]arr, int r) {

		int sum = 0;

		for (int i =0 ;i<6;i++){
			sum = sum + arr[r][i];
		}		
		return sum;
	}
	public static int colSum(int[][]arr, int c) {
		int sum = 0;
		for (int i =0 ;i<6;i++){
			sum = sum + arr[i][c];
		}
		return sum;
	}
}
