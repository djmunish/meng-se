package testJava;

import java.util.Scanner;

public class org {
public static void main(String[] a) {
	Scanner s = new Scanner(System.in);
	
	int R = s.nextInt();
	int C = s.nextInt();
	String oS = s.nextLine();

	for (int i= 0;i<R;i++) {
		oS += s.nextLine();
	}
	int S = s.nextInt();
	String[] oArr = oS.split("");
	
	int parseInt = 0;
	
	String[][] grid = new String[R][C];
	String[][] upGrid = new String[R][C];
	for (int i = 0; i<R;i++) {
		for (int j =0; j<C;j++) {
			grid[i][j] = oArr[parseInt];
			upGrid[i][j] = oArr[parseInt];
			parseInt++;
		}
	}
	
	int countEmpty = 0;

	for (int x = 0; x<S ; x++) {
	
		
		
		for (int r = 0; r<R ; r++) {
		for (int c = 0; c<C ; c++) {
			
			if (grid[r][c].equals("#")){
				//empty
				if (r > 0 && r < R-1 && c > 0 && c < C-1){
					
					countEmpty = countN(grid,r,c,R,C);
					if (countEmpty == 3) {
						countEmpty = 0;
						upGrid[r][c] = "@";
					}
				
			}
				else if ((r == 0 && c == 0) || (r == 0 && c == C-1) || (r == R-1 && c == 0) || (r == R-1 && c == C-1)) {
						countEmpty = countCornerN(grid,r,c,R,C);
						if (countEmpty == 3) {
							countEmpty = 0;
							upGrid[r][c] = "@";
						}
					
				}
				else {
					countEmpty = fiveNCount(grid,r,c,R,C);
					if (countEmpty == 3) {
						countEmpty = 0;
						upGrid[r][c] = "@";
					}
				}
				}
			else {
				//organism
				if (r > 0 && r < R-1 && c > 0 && c < C-1){
					countEmpty = countN(grid,r,c,R,C);
					if (countEmpty <= 1 ||  countEmpty >= 4) {
						upGrid[r][c] = "#";
					}
				
			}
				else if ((r == 0 && c == 0) || (r == 0 && c == C-1) || (r == R-1 && c == 0) || (r == R-1 && c == C-1)) {
					countEmpty = countCornerN(grid,r,c,R,C);
					if (countEmpty <= 1 ||  countEmpty >= 4) {
						upGrid[r][c] = "#";
					}
				
			}
			else {
				countEmpty = fiveNCount(grid,r,c,R,C);
				if (countEmpty <= 1 ||  countEmpty >= 4) {
					upGrid[r][c] = "#";
				}
			}
		}
	}
	}
		
		
		
//	grid = upGrid;
	
	
	for (int i = 0; i<R;i++) {
		for (int j =0; j<C;j++) {
			grid[i][j] = upGrid[i][j];
		}
	}
	
	
	System.out.println("======newGrid=======");

	for (int i = 0; i<R;i++) {
		for (int j =0; j<C;j++) {
			System.out.print(grid[i][j]);
		}
		System.out.println();
	}
	}
	int finlcount= 0 ;
	for (int i = 0; i<R;i++) {
		for (int j =0; j<C;j++) {
			if (upGrid[i][j].equals("@")) {
				finlcount++;
			}
			System.out.print(upGrid[i][j]);
		}
		System.out.println();
	}
	System.out.println(finlcount);
	



	s.close();
}

public static int fiveNCount(String[][] grid, int r, int c, int R, int C) {
	int countEmpty = 0;
	System.out.println("======processsing grid=======");


	if (r == 0) {
		if (grid[r][c-1].equals("@")) {
			countEmpty++;
		}
		if (grid[r][c+1].equals("@")) {
			countEmpty++;
		}
		if (grid[r+1][c-1].equals("@")) {
			countEmpty++;
		}
		if (grid[r+1][c+1].equals("@")) {
			countEmpty++;
		}
		if (grid[r+1][c].equals("@")) {
			countEmpty++;
		}
	}
	if(r == R-1){
		if (grid[r][c-1].equals("@")) {
			countEmpty++;
		}
		if (grid[r][c+1].equals("@")) {
			countEmpty++;
		}
		if (grid[r-1][c-1].equals("@")) {
			countEmpty++;
		}
		if (grid[r-1][c+1].equals("@")) {
			countEmpty++;
		}
		if (grid[r-1][c].equals("@")) {
			countEmpty++;
		}
		
		
		
		System.out.println("count==="+countEmpty);
	}
	if (c == 0) {
		if (grid[r+1][c].equals("@")) {
			countEmpty++;
		}
		if (grid[r-1][c].equals("@")) {
			countEmpty++;
		}
		if (grid[r-1][c+1].equals("@")) {
			countEmpty++;
		}
		if (grid[r+1][c+1].equals("@")) {
			countEmpty++;
		}
		if (grid[r][c+1].equals("@")) {
			countEmpty++;
		}
	}
	if(c == C-1){
		if (grid[r+1][c].equals("@")) {
			countEmpty++;
		}
		if (grid[r-1][c].equals("@")) {
			countEmpty++;
		}
		if (grid[r-1][c-1].equals("@")) {
			countEmpty++;
		}
		if (grid[r+1][c-1].equals("@")) {
			countEmpty++;
		}
		if (grid[r][c-1].equals("@")) {
			countEmpty++;
		}
	}
	return countEmpty;
}

public static int countCornerN(String[][] grid, int r, int c, int R, int C) {
	int countEmpty = 0;
	System.out.println("======processsing grid corners=======");

		//empty
		if (r == 0 && c == 0){
		if (grid[r][c+1].equals("@")) {
			countEmpty++;
		}
		if (grid[r+1][c].equals("@")) {
			countEmpty++;
		}
		if (grid[r+1][c+1].equals("@")) {
			countEmpty++;
		}	
		}
		else if (r == R-1 && c == 0){
			if (grid[r][c+1].equals("@")) {
				countEmpty++;
			}
			if (grid[r-1][c].equals("@")) {
				countEmpty++;
			}
			if (grid[r-1][c+1].equals("@")) {
				countEmpty++;
			}	
			}
		else if (r == R-1 && c == C-1){
			if (grid[r][c-1].equals("@")) {
				countEmpty++;
			}
			if (grid[r-1][c].equals("@")) {
				countEmpty++;
			}
			if (grid[r-1][c-1].equals("@")) {
				countEmpty++;
			}	
			}
		else if (r == 0 && c == C-1){
			if (grid[r][c-1].equals("@")) {
				countEmpty++;
			}
			if (grid[r+1][c].equals("@")) {
				countEmpty++;
			}
			if (grid[r+1][c-1].equals("@")) {
				countEmpty++;
			}	
			}
		
	
	return countEmpty;
}

public static int countN(String[][] grid, int r, int c, int R, int C) {
	int countEmpty = 0;
	System.out.println("======processsing grid centers=======");

		//empty
		if (r > 0 && r < R-1 && c > 0 && c < C-1){
		if (grid[r][c+1].equals("@")) {
			countEmpty++;
		}
		if (grid[r][c-1].equals("@")) {
			countEmpty++;
		}
		if (grid[r-1][c].equals("@")) {
			countEmpty++;
		}
		if (grid[r+1][c].equals("@")) {
			countEmpty++;
		}
		if (grid[r+1][c+1].equals("@")) {
			countEmpty++;
		}
		if (grid[r-1][c+1].equals("@")) {
			countEmpty++;
		}
		if (grid[r+1][c-1].equals("@")) {
			countEmpty++;
		}
		if (grid[r-1][c-1].equals("@")) {
			countEmpty++;
		}		
		}
	return countEmpty;
}

}
