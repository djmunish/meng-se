package PrintingNames;

import java.util.ArrayList;
import java.util.Scanner;

public class SnakeLadder {

	static int changeLocation(int presentLocation,int changeby) {
		return presentLocation+changeby;
	}
	
	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		int noOfDetails;
		noOfDetails=Integer.parseInt(scanner.nextLine());
		String temp;
		int[] details=new int[2];
		ArrayList<Snake> snake=new ArrayList<Snake>();
		ArrayList<Ladder> ladder= new ArrayList<Ladder>();
		for(int i=0;i<noOfDetails;i++) {
			temp=scanner.nextLine();
			String[] CellDetails=temp.split("\\s+");
			details[0]=Integer.parseInt(CellDetails[0]);
			details[1]=Integer.parseInt(CellDetails[1]);
			if(details[0]<details[1]) 
				ladder.add(new Ladder(details[0],details[1]));
			if(details[0]>details[1])
				snake.add(new Snake(details[0],details[1]));
		}
		int noOfMoves;
		noOfMoves=Integer.parseInt(scanner.nextLine().trim());
		temp= scanner.nextLine();
		String[] moveDetails=temp.split("\\s+");
		Player a=new Player(1);
		Player b=new Player(1);
		for(int i=0;i<noOfMoves;i++) {
			if(i%2==0) {
				a.location=changeLocation(a.location,Integer.parseInt(moveDetails[i]));
				for(Ladder l1: ladder) {
					if(a.location==l1.startLocation) {
						a.location=l1.endLocation;
						for(Snake s1: snake) {
							if(a.location==s1.startLocation)
								a.location=s1.endLocation;
						}
				}
			}
				for(Snake s1: snake) {
					if(a.location== s1.startLocation) {
						a.location=s1.endLocation;
						for(Ladder l1: ladder) {
							if(a.location==l1.startLocation)
								a.location=l1.endLocation;
						}
					}
				}
				if(a.location==100) {
					//System.out.println("A is winner and position is : "+ a.location);
					break;
				}
			}
			else {
				b.location=changeLocation(b.location,Integer.parseInt(moveDetails[i]));
				for(Ladder l1: ladder) {
					if(b.location==l1.startLocation) {
						b.location=l1.endLocation;
						for(Snake s1: snake) {
							if(b.location==s1.startLocation)
								b.location=s1.endLocation;
						}
				}
			}
				for(Snake s1: snake) {
					if(b.location== s1.startLocation) {
						b.location=s1.endLocation;
						for(Ladder l1: ladder) {
							if(b.location==l1.startLocation)
								b.location=l1.endLocation;
						}
					}
				}
			}
			if(b.location==100) {
				//System.out.println("B is winner and position is : "+ b.location);
				break;
			}
		}
		
		if(a.location>b.location)
			System.out.println("A"+" "+a.location);
		else
			System.out.println("B"+" "+b.location);

	}

}

class Snake{
	int startLocation;
	int endLocation;
	Snake(int startLocation,int endLocation){
		this.startLocation=startLocation;
		this.endLocation=endLocation;
	}
}
class Ladder{
	int startLocation;
	int endLocation;
	Ladder(int startLocation,int endLocation){
		this.startLocation=startLocation;
		this.endLocation=endLocation;
	}
}
class Player{
	int location;
	Player(int location){
		this.location=location;
	}
}