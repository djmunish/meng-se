package PrintingNames;

import java.util.Collections;
import java.util.Scanner;

public class exam7 {

	public static void main(String[] args) {
	
		Scanner sc=new Scanner(System.in);
		String drink,name,size=null;
		int[] cupsizes=new int[4];
		String[] cupDetails;
		int i =0;
		String temp;
		temp=sc.nextLine();
		cupDetails=temp.split("\\s+");
		for(String x:cupDetails) {
			cupsizes[i]=Integer.parseInt(x);
			i++;
		}
		int noOfCustomers=Integer.parseInt(sc.nextLine());
		String[] customerName=new String[noOfCustomers];
		int[] customerAge=new int[noOfCustomers];
		int[] customerHt=new int[noOfCustomers];
		int[] customerInch=new int[noOfCustomers];
		String[] customerDetails;
		for(i=0;i<noOfCustomers;i++) {
			temp=sc.nextLine();
			cupDetails=temp.split("\\s+");
			customerName[i]=cupDetails[0];
			customerAge[i]=Integer.parseInt(cupDetails[1]);
			customerHt[i]=Integer.parseInt(cupDetails[2]);
			customerInch[i]=Integer.parseInt(cupDetails[3]);
		}
		
		for(i=0;i<noOfCustomers;i++) {
			if(customerAge[i]<18) {
				drink="Water";
			}
			else
				drink="Beer";
			double calculateSize= customerHt[i]*10 + customerInch[i]*8/12;
			if(calculateSize<=cupsizes[0])
				size="ExtraSmall";
			else if(calculateSize>cupsizes[0] && calculateSize<=cupsizes[1])
				size="Small";
			else if(calculateSize>cupsizes[1] && calculateSize<=cupsizes[2])
				size="Medium";
			else 
				size="Large";
	
			System.out.println(customerName[i]+" "+drink+" "+size+" "+customerAge[i]+" "+customerHt[i]+" "+customerInch[i]);
		}
		

	}

}
