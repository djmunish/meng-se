package PrintingNames;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

public class StronglyConnectedCompo {
	
	HashMap<Integer, ArrayList<Integer>> hash=new HashMap<>();
	HashMap<Integer, ArrayList<Integer>> reverse=new HashMap<>();
		
		public void addEdge(int x, int y) {
			if(hash.containsKey(x)) {
				ArrayList<Integer> values=hash.get(x);
				if(!values.contains(y)) {
				values.add(y);
				hash.put(x, values);}
			}
			else {
				ArrayList<Integer> values=new ArrayList<>();
				values.add(y);
				hash.put(x, values);
			}	
		}
		
		public void getTranspose() {
			for(Integer x: hash.keySet()) {
				ArrayList<Integer> temp1=hash.get(x);
				if(temp1!=null) {
					for(int i=0;i<temp1.size();i++) {
						if(reverse.containsKey(temp1.get(i))) {
							ArrayList<Integer> values=reverse.get(temp1.get(i));
							if(!values.contains(x)) {
							values.add(x);
							reverse.put(temp1.get(i), values);}
						}
						else {
							ArrayList<Integer> values=new ArrayList<>();
							values.add(x);
							reverse.put(temp1.get(i), values);
						}	
					}	
			
				}
				
			}
		}
		
		ArrayList<Integer> visited= new ArrayList<>();
		ArrayList<Integer> reverseVisited= new ArrayList<>();
		ArrayList<Integer> result= new ArrayList<>();
		static int count=0;
		//Map.Entry<Integer,ArrayList<Integer>> entry = hash.entrySet().iterator().next();
		public void connectedCompo() {
			ArrayList<Integer> stack= new ArrayList<>();
			for(Integer x : hash.keySet()) {
				if(!visited.contains(x))
					DFS(x,stack,visited);
			}
			getTranspose();
			
		    
			for(int i=stack.size()-1;i>=0;i--) {
				if(!reverseVisited.contains(stack.get(i))) {
					DFSReverse(stack.get(i), reverseVisited);
					result.sort(null);
					for(int j=0;j<result.size();j++) {
						if(j==0)
							System.out.print("[");
						System.out.print(result.get(j));
						if(j==result.size()-1)
							System.out.print("]");
						else
							System.out.print(",");
					}
					System.out.println();
					result.clear();	
					count++;
				}
				
			}
			
		}
		
		public void DFS(int begin, ArrayList<Integer> stack, ArrayList<Integer> visited) {
			visited.add(begin);
			ArrayList<Integer> lst= hash.get(begin);
			if(lst!=null) {
				for(Integer x: lst) {
					if(!visited.contains(x))
						DFS(x,stack, visited);		
				}
			}
			stack.add(begin);
		}
		
		public void DFSReverse(int begin, ArrayList<Integer> visited) {
			visited.add(begin);
			ArrayList<Integer> lst= reverse.get(begin);
			if(lst!=null) {
				for(Integer x: lst) {
					if(!visited.contains(x))
						DFSReverse(x, visited);
				}
			}
			result.add(begin);
		}
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		//	int noOfTests= Integer.parseInt(sc.nextLine());
			StronglyConnectedCompo ob=new StronglyConnectedCompo();
			//String[] results=new String[noOfTests];
			//for(int i=0;i<noOfTests;i++) {
			int noOfLines=Integer.parseInt(sc.nextLine());
				//int noOfEdges=Integer.parseInt(sc.nextLine());
				for(int j=0;j<noOfLines;j++) {
				String temp=sc.nextLine();
				String[] tempArr=temp.split("\\s+");
				ob.addEdge(Integer.parseInt(tempArr[0]), Integer.parseInt(tempArr[1]));
				}
				
				ob.connectedCompo();
				System.out.println(count);

	}	

}
