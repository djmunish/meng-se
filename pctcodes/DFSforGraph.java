package PrintingNames;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;

public class DFSforGraph {
	
	HashMap<Integer,ArrayList<Integer>> hash=new HashMap<>();

	public void addEdge(int node1, int node2) {
		if(hash.containsKey(node1)) {
			ArrayList<Integer> values=hash.get(node1);
			values.add(node2);
			hash.put(node1, values);
		}
		else {
			ArrayList<Integer> values=new ArrayList<>();
			values.add(node2);
			hash.put(node1, values);
		}
	}
	
	public void DFS(int begin) {
		if(hash.isEmpty())
			return;
		ArrayList<Integer> visited=new ArrayList<>();
		DFS(begin, visited);
		
	}
	
	public void DFS(int begin, ArrayList<Integer> visited) {
		visited.add(begin);
		System.out.print(begin+" ");
		ArrayList<Integer> lst=hash.get(begin);
		if(lst!=null) {
		for(int i=0;i<lst.size();i++) {
			if(!visited.contains(lst.get(i)))
				DFS(lst.get(i), visited);
		}
	}
	}
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int noOfLines=Integer.parseInt(sc.nextLine());
		DFSforGraph ob=new DFSforGraph();
		for(int j=0;j<noOfLines;j++) {
			String temp=sc.nextLine();
			String[] tempArr=temp.split("\\s+");
			int[] actualNos=new int[tempArr.length];
			for(int i=0;i<tempArr.length;i++)
				actualNos[i]=Integer.parseInt(tempArr[i]);
			
			ob.addEdge(actualNos[0], actualNos[1]);
		}
		Map.Entry<Integer,ArrayList<Integer>> entry = ob.hash.entrySet().iterator().next();
		int x=entry.getKey();
		ob.DFS(x);

	}

}
