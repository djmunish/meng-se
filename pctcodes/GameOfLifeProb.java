import java.util.Scanner;

public class GameOfLifeProb {
	static Character[][] array;
	static int m,n;
  public static void main(String[] args){
	  GameOfLifeProb obj=new GameOfLifeProb();
	  Scanner s1=new Scanner(System.in);
	  Scanner s2=new Scanner(System.in);
	  System.out.println("Enter m and n");
	  m=s1.nextInt();
	  n=s1.nextInt();
	  array=new Character[m][n];
	  System.out.println("Enter values of chars");
	  for(int i=0;i<m;i++){
			  String inp=s2.nextLine();
			  for(int j=0;j<n;j++){
			  array[i][j]=inp.charAt(j);
		  }
	  }
	 // System.out.print(obj.numberofNeighbor(0, 1));
	  //obj.printarray();
	  obj.printarray();
	  for(int i=0;i<m;i++){
		  for(int j=0;j<n;j++){
	  System.out.print(obj.numberofNeighbor(i, j));
		  }
		  System.out.println();
		  }
	  obj.generationChange();
	  obj.printarray();
	  
  }
  public void generationChange(){
	  for(int p=0;p<m;p++){
		  for(int q=0;q<n;q++){
		Character status=array[p][q];
//		System.out.println("Value of p is "+p+" Value of q is "+ q);
		printarray();
		int neighbour	=numberofNeighbor(p,q);
		
//		System.out.println("Current status is "+ status+" Neighbours are "+neighbour);
		System.out.print(neighbour);
		if(status.equals('#')){              //if cell is unoccupied
		 if(neighbour==3){
			 array[p][q]='@';
//			 System.out.println("Changed status is "+ array[p][q]);
		 }
		}
		else if(status.equals('@')){								//if cell is occupied
			 if(neighbour<2){
				 array[p][q]='#';
			 }
			 else if(neighbour>3){
				 array[p][q]='#';
			 }
			 else if(neighbour==2){
				 array[p][q]='@';
			 }
			 else if(neighbour==3){
				 array[p][q]='@';
			 }
//			 System.out.println("Changed status is "+ array[p][q]);
		}
		System.out.println();
		  }
		 }
  }
  
  
  public int numberofNeighbor(int i,int j){
	  int neighbours=0;
	  if(i!=0&&j!=0&&array[i-1][j-1]=='@'){
		  neighbours++;
	  }
	  if(i!=0&&array[i-1][j]=='@'){
		  neighbours++;
	  }
	  if(i!=0&&!(j>n-2)&&array[i-1][j+1]=='@'){
		  neighbours++;
	  }
	  if(j!=0&&array[i][j-1]=='@'){
		  neighbours++;
	  }
	  if(!(j>n-2)&&array[i][j+1]=='@'){
		  neighbours++;
	  }
	  if(!(i>m-2)&&j!=0&&array[i+1][j-1]=='@'){
		  neighbours++;
	  }
	  if(!(i>m-2)&&array[i+1][j]=='@'){
		  neighbours++;
	  }
	  if(!(i>m-2)&&!(j>n-2)&&array[i+1][j+1]=='@'){
		  neighbours++;
	  }
	  
	  return neighbours;
  }
  
  public void printarray(){
	  for(int i=0;i<m;i++){
		  for(int j=0;j<n;j++){
			 System.out.print(array[i][j]);
  }
		  System.out.println();
}
}
  }




/*
#@@
@@#
##@
 */
 