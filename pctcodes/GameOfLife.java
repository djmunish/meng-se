package PrintingNames;

import java.util.Scanner;

//A simple Java program to implement Game of Life
public class GameOfLife
{
 public static void main(String[] args)
 {
     Scanner scanner=new Scanner(System.in);
     String input= scanner.nextLine();
     String[] labels=input.split("\\s+");
     int rows= Integer.parseInt(labels[0]);
     int cols= Integer.parseInt(labels[1]);
     char[][] grid= new char[rows][cols];
     if(rows<=30 && cols<=30)
     {
    	 for(int i=0;i<rows;i++)
    	 {
    		 String description=scanner.nextLine();
    		 for(int j=0;j<description.length();j++)
    		 {
    			 grid[i][j]=description.charAt(j);
    		 }
    	 }
     }
     nextGeneration(grid, rows, cols);
 }
 static void nextGeneration(char grid[][], int rows, int cols)
 {
     char[][] future = new char[rows][cols];
     for (int l = 1; l < rows - 1; l++)
     {
         for (int m = 1; m < cols - 1; m++)
         {
             // finding no Of Neighbours that are alive
             int aliveNeighbours = 0;
             for (int i = -1; i <= 1; i++)
                 for (int j = -1; j <= 1; j++)
                     aliveNeighbours += grid[l + i][m + j];

             // The cell needs to be subtracted from
             // its neighbours as it was counted before
             aliveNeighbours -= grid[l][m];

             // Implementing the Rules of Life

             // Cell is lonely and dies
             if ((grid[l][m] == 1) && (aliveNeighbours < 2))
                 future[l][m] = '#';

             // Cell dies due to over population
             else if ((grid[l][m] == 1) && (aliveNeighbours > 3))
                 future[l][m] = '#';

             // A new cell is born
             else if ((grid[l][m] == 0) && (aliveNeighbours == 3))
                 future[l][m] = '@';

             // Remains the same
             else
                 future[l][m] = grid[l][m];
         }
     }
     int count=0;
     for (int i = 0; i < rows; i++)
     {
         for (int j = 0; j < cols; j++)
         {
             if(future[i][j]=='@')
            	 count++;
         }
     }
     System.out.println(count);
 }
}