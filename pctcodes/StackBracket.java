package PrintingNames;

import java.util.Scanner;

public class StackBracket {

	static char[] stack;
	static int top=-1;
	public void push(char bracket) {
		stack[++top]=bracket;
	}
	 
	public char pop() {
		return stack[top--];
	}
	
	public static void main(String arg[]) {
	Scanner sc=new Scanner(System.in);
	int n = Integer.parseInt(sc.nextLine());
	StackBracket s=new StackBracket();
	String[] allStrings=new String[n];
	for(int i=0;i<n;i++) {
		String temp= sc.nextLine();
		allStrings[i]=temp;
	}
	for(String x: allStrings) {
		stack=new char[x.length()];
		for(int i=0;i<x.length();i++) {
			if(x.charAt(i)=='('|| x.charAt(i)=='['||x.charAt(i)=='{') {
				s.push(x.charAt(i));
			}
			if(x.charAt(i)==')'|| x.charAt(i)==']'||x.charAt(i)=='}') {
				char b= s.pop();
				char y=x.charAt(i);
				if(b!='(' && y==')'|| b!='{' && y=='}'||b!='[' && y==']') {
					System.out.println("FALSE");
					continue;
				}	
			}
		}
		if(top==-1)
			System.out.println("TRUE");	
		else
			System.out.println("FALSE");
	}
}
}