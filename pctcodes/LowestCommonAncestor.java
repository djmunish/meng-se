package PrintingNames;

import java.util.Scanner;
import java.lang.Math.*;

import PrintingNames.BinarySearchTree.Node;

public class LowestCommonAncestor {
	
	public class Node{
		int name;
		Node leftChild;
		Node righChild;
		Node parent;
		Node(int name){
			this.name=name;
		}
	}
	Node root;
	public void addNode(int num) {
		root = addNode(root,num);
	}
	
	public Node addNode(Node root, int value) {
		if(root == null)
			root= new Node(value);
		else {
			if(root.name> value)
				root.leftChild=addNode(root.leftChild, value);
			if(root.name < value)
				root.righChild = addNode(root.righChild, value);
		}
		return root;
	}
	
	public Node lowComAnc(Node root,int n1, int n2) {
		if(root.name> Math.max(n1,n2))
			return lowComAnc(root.leftChild,n1, n2);
		if(root.name<Math.min(n1, n2))
			return lowComAnc(root.righChild, n1, n2);
		else
			return root;			
	}
	
	

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String temp=sc.nextLine();
		String[] tempArr=temp.split("\\s+");
		int[] actualNums=new int[tempArr.length];
		LowestCommonAncestor ob= new LowestCommonAncestor();
		for(int i=0;i<tempArr.length;i++) {
			actualNums[i]=Integer.parseInt(tempArr[i]);
			ob.addNode(actualNums[i]);
		}
		String temp2=sc.nextLine();
		String[] tempArr2=temp2.split("\\s+");
		
		Node x=ob.lowComAnc(ob.root, Integer.parseInt(tempArr2[0]) ,Integer.parseInt(tempArr2[1]));
		System.out.println(x.name);
		

	}

}
