package PrintingNames;

import java.util.Scanner;

public class exam4 {

	public static void main(String[] args) {
		
		Scanner sc= new Scanner(System.in);
		String temp = sc.nextLine();
		String[] grid=temp.split("\\s+");
		int rows=Integer.parseInt(grid[0]);
		int columns=Integer.parseInt(grid[1]);
		
		if(rows<=30 && columns<=30) {
		char[][] cellDetail=new char[rows][columns];
		for(int i=0; i<rows;i++) {
			temp=sc.nextLine();
			//String[] tempdetails= temp.split("\\s+");
			for(int j=0; j<columns;j++) {
				cellDetail[i][j]= temp.charAt(j);
			}
		}
		int noOfGenerations= Integer.parseInt(sc.nextLine());
		
		int[][] cellCount=new int[rows][columns];
		
		for(int n=0;n<noOfGenerations;n++) {
		
			for(int i=0;i<rows;i++) {
			for(int j=0;j<columns;j++) {
				int count=0;
				for(int k=-1;k<=1;k++) {
					
					for(int l=-1;l<=1;l++) {
						if(i+k<0|| j+l<0 || i+k>=rows|| j+l>=columns)
							continue;
						else {
							if(cellDetail[i+k][j+l]=='@')
								count++;}
					}
				}
				if(cellDetail[i][j]=='@')
				cellCount[i][j]=count-1;
				else
					cellCount[i][j]=count;
			}
			}
		for(int i=0;i<rows;i++) {
			for(int j=0;j<columns;j++) {
				if(cellDetail[i][j]=='@') {
				if(cellCount[i][j]==0||cellCount[i][j]==1)
					cellDetail[i][j]='#';
				else if(cellCount[i][j]>=4)
					cellDetail[i][j]='#';
				else if(cellCount[i][j]==2||cellCount[i][j]==3)
					cellDetail[i][j]='@';
				}
				else {
					if(cellCount[i][j]==3)
						cellDetail[i][j]='@';
				}
			}

		}
   }
		int finalcount=0;
		for(int i=0;i<rows;i++) {
			for(int j=0;j<columns;j++) {
				if(cellDetail[i][j]=='@')
					finalcount++;
			}
		}
		System.out.println(finalcount);
}
}
}